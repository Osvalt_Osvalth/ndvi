# -*- coding: utf-8 -*-
"""
Created on Mon Jul  6 12:11:39 2020

@author: music
"""
import cv2
import numpy as np
from matplotlib import pyplot as plt

from NDVI_files import leerIMGs_TIF as ReadWrittenIMGs
from NDVI_files import rutas_D_Archivos as myPath

def getPathBynaryByName(nameImage , histo):
    guardarRutaCategoria = myPath.getPathByCategory(nameImage, histo)
    return guardarRutaCategoria

def getclustering ( ImG_Original, kmeans ):
    imgORI = cv2.imread( ImG_Original )
    while(1):
        nameImg, nameHistoCluster = getPathBynaryByName('CLUSTERING', histo = 'HISTOGRAMA_CLUSTER')
        
        img = np.array(imgORI)
        Z = img.reshape((-1,3))
        Z = np.float32(Z)# convert to np.float32
        # Se definen los cricterios, número de clusters(K) y se aplica kmeans()
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        
        ret, label, center = cv2.kmeans( Z, kmeans, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
        # Reconvertimos en uint8, y creamos la imagen original
        center = np.uint8(center)
        res = center[label.flatten()]
        resultado = res.reshape((img.shape))
        # Guardamos imagen
        ReadWrittenIMGs.writteImgBlack( nameImg, resultado )
        # Separación de datos
        LABEL= label
        
        
        A = Z[label.ravel()==0]
        B = Z[label.ravel()==1]
        C = Z[label.ravel()==3]
        # Se plotean los datos
        plt.scatter(A[:,0],A[:,1])
        plt.scatter(B[:,0],B[:,1],c = 'r')
        plt.scatter(center[:,0],center[:,1],s = 80,c = 'y', marker = 's')
        plt.xlabel('Color'),plt.ylabel('Peso')
        plt.title('Clustering')
        plt.savefig(nameHistoCluster,dpi=256)#2600
        plt.show()
        plt.close(1)
        
        return nameImg, nameHistoCluster 
    
    
