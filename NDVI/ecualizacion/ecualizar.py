# -*- coding: utf-8 -*-
"""
Created on Sun Jun 28 01:07:52 2020

@author: music
"""
import cv2

from NDVI_files import leerIMGs_TIF as ReadWrittenIMGs
from NDVI_files import ndvi as ReadIMGs
from NDVI_files import rutas_D_Archivos as myPath
from HISTOGRAMA import histograma2 as hist

# https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_thresholding/py_thresholding.html
def getPathBynaryByName(nameImage , histo):
    guardarRutaCategoria = myPath.getPathByCategory(nameImage, histo)
    return guardarRutaCategoria

def ecualizarHistograma ( imagen ):
    img = ReadIMGs.obtenerImgNIR( imagen )
    while(1):
        nameImg, nameHisto = getPathBynaryByName('ECUALIZACION', histo = 'HISTOGRAMA')
        img = cv2.equalizeHist( img )
        ReadWrittenIMGs.writteImgBlack( nameImg, img )
        nameHisto2 = hist.getHistogram ( nameImg, nameHisto )
        return nameImg, nameHisto2