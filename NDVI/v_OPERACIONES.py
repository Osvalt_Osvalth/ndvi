# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventana_OPERACIONES.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QPushButton
from NDVI_files import rutas_D_Archivos as paths
from OPERACIONES import operaciones as oper2
from TipoIMG import configuracion as extentionImg

class Ui_VENTANA_OPERACIONES(object):
    def setupUi(self, VENTANA_OPERACIONES):
        VENTANA_OPERACIONES.setObjectName("VENTANA_OPERACIONES")
        VENTANA_OPERACIONES.resize(750, 518)
        VENTANA_OPERACIONES.setMaximumSize(QtCore.QSize(750, 518))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        VENTANA_OPERACIONES.setFont(font)
        VENTANA_OPERACIONES.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        VENTANA_OPERACIONES.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.centralwidget = QtWidgets.QWidget(VENTANA_OPERACIONES)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox_ACCIONES = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_ACCIONES.setGeometry(QtCore.QRect(10, -5, 731, 111))
        self.groupBox_ACCIONES.setObjectName("groupBox_ACCIONES")
        self.btn_ABRIRIMAGEN_A = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_ABRIRIMAGEN_A.setGeometry(QtCore.QRect(10, 20, 111, 30))
        self.btn_ABRIRIMAGEN_A.setObjectName("btn_ABRIRIMAGEN_A")
        self.btn_OPERAR = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_OPERAR.setGeometry(QtCore.QRect(610, 70, 111, 30))
        self.btn_OPERAR.setObjectName("btn_OPERAR")
        self.stackedWidget_SUMAS_RESTAS = QtWidgets.QStackedWidget(self.groupBox_ACCIONES)
        self.stackedWidget_SUMAS_RESTAS.setEnabled(True)
        self.stackedWidget_SUMAS_RESTAS.setGeometry(QtCore.QRect(260, 20, 211, 81))
        self.stackedWidget_SUMAS_RESTAS.setFrameShape(QtWidgets.QFrame.Box)
        self.stackedWidget_SUMAS_RESTAS.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.stackedWidget_SUMAS_RESTAS.setObjectName("stackedWidget_SUMAS_RESTAS")
        self.PAGE_SUMA = QtWidgets.QWidget()
        self.PAGE_SUMA.setObjectName("PAGE_SUMA")
        self.radioButton_ADD = QtWidgets.QRadioButton(self.PAGE_SUMA)
        self.radioButton_ADD.setGeometry(QtCore.QRect(10, 25, 51, 20))
        self.radioButton_ADD.setChecked(True)
        self.radioButton_ADD.setObjectName("radioButton_ADD")
        self.radioButton_ADDWEIGHTED = QtWidgets.QRadioButton(self.PAGE_SUMA)
        self.radioButton_ADDWEIGHTED.setGeometry(QtCore.QRect(80, 25, 111, 20))
        self.radioButton_ADDWEIGHTED.setObjectName("radioButton_ADDWEIGHTED")
        self.stackedWidget_SUMAS_RESTAS.addWidget(self.PAGE_SUMA)
        self.page_RESTA = QtWidgets.QWidget()
        self.page_RESTA.setObjectName("page_RESTA")
        self.radioButton_ABSDIFF = QtWidgets.QRadioButton(self.page_RESTA)
        self.radioButton_ABSDIFF.setGeometry(QtCore.QRect(16, 25, 71, 20))
        self.radioButton_ABSDIFF.setChecked(True)
        self.radioButton_ABSDIFF.setObjectName("radioButton_ABSDIFF")
        self.radioButton_SUBTRACT = QtWidgets.QRadioButton(self.page_RESTA)
        self.radioButton_SUBTRACT.setGeometry(QtCore.QRect(110, 25, 81, 20))
        self.radioButton_SUBTRACT.setObjectName("radioButton_SUBTRACT")
        self.stackedWidget_SUMAS_RESTAS.addWidget(self.page_RESTA)
        self.groupBox_ELECCION = QtWidgets.QGroupBox(self.groupBox_ACCIONES)
        self.groupBox_ELECCION.setGeometry(QtCore.QRect(130, 20, 121, 81))
        self.groupBox_ELECCION.setTitle("")
        self.groupBox_ELECCION.setObjectName("groupBox_ELECCION")
        self.radioButton_VENTANA2_RESTA = QtWidgets.QRadioButton(self.groupBox_ELECCION)
        self.radioButton_VENTANA2_RESTA.setGeometry(QtCore.QRect(10, 40, 101, 21))
        self.radioButton_VENTANA2_RESTA.setChecked(False)
        self.radioButton_VENTANA2_RESTA.setAutoExclusive(True)
        self.radioButton_VENTANA2_RESTA.setObjectName("radioButton_VENTANA2_RESTA")
        self.radioButton_VENTANA1_SUMA = QtWidgets.QRadioButton(self.groupBox_ELECCION)
        self.radioButton_VENTANA1_SUMA.setGeometry(QtCore.QRect(12, 10, 71, 21))
        self.radioButton_VENTANA1_SUMA.setChecked(True)
        self.radioButton_VENTANA1_SUMA.setObjectName("radioButton_VENTANA1_SUMA")
        self.btn_ABRIRIMAGEN_B = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_ABRIRIMAGEN_B.setGeometry(QtCore.QRect(10, 70, 111, 30))
        self.btn_ABRIRIMAGEN_B.setObjectName("btn_ABRIRIMAGEN_B")
        self.groupBox_ALPHA_BETHA = QtWidgets.QGroupBox(self.groupBox_ACCIONES)
        self.groupBox_ALPHA_BETHA.setEnabled(False)
        self.groupBox_ALPHA_BETHA.setGeometry(QtCore.QRect(480, 20, 121, 81))
        self.groupBox_ALPHA_BETHA.setTitle("")
        self.groupBox_ALPHA_BETHA.setFlat(False)
        self.groupBox_ALPHA_BETHA.setCheckable(False)
        self.groupBox_ALPHA_BETHA.setChecked(False)
        self.groupBox_ALPHA_BETHA.setObjectName("groupBox_ALPHA_BETHA")
        self.label_ALPHA = QtWidgets.QLabel(self.groupBox_ALPHA_BETHA)
        self.label_ALPHA.setGeometry(QtCore.QRect(20, 8, 30, 30))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.label_ALPHA.setFont(font)
        self.label_ALPHA.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.label_ALPHA.setScaledContents(True)
        self.label_ALPHA.setObjectName("label_ALPHA")
        self.label_BETHA = QtWidgets.QLabel(self.groupBox_ALPHA_BETHA)
        self.label_BETHA.setGeometry(QtCore.QRect(20, 44, 30, 30))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.label_BETHA.setFont(font)
        self.label_BETHA.setObjectName("label_BETHA")
        self.doubleSpinBox_ALPHA = QtWidgets.QDoubleSpinBox(self.groupBox_ALPHA_BETHA)
        self.doubleSpinBox_ALPHA.setGeometry(QtCore.QRect(50, 10, 50, 30))
        self.doubleSpinBox_ALPHA.setDecimals(2)
        self.doubleSpinBox_ALPHA.setMinimum(0.0)
        self.doubleSpinBox_ALPHA.setMaximum(1.0)
        self.doubleSpinBox_ALPHA.setSingleStep(0.1)
        self.doubleSpinBox_ALPHA.setObjectName("doubleSpinBox_ALPHA")
        self.doubleSpinBox_BETHA = QtWidgets.QDoubleSpinBox(self.groupBox_ALPHA_BETHA)
        self.doubleSpinBox_BETHA.setGeometry(QtCore.QRect(50, 45, 50, 30))
        self.doubleSpinBox_BETHA.setDecimals(2)
        self.doubleSpinBox_BETHA.setMinimum(0.0)
        self.doubleSpinBox_BETHA.setMaximum(1.0)
        self.doubleSpinBox_BETHA.setSingleStep(0.1)
        self.doubleSpinBox_BETHA.setObjectName("doubleSpinBox_BETHA")
        self.groupBox_IMG_RESULTANTE = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_IMG_RESULTANTE.setGeometry(QtCore.QRect(10, 115, 731, 381))
        self.groupBox_IMG_RESULTANTE.setObjectName("groupBox_IMG_RESULTANTE")
        self.tabWidget_OPERACIONES = QtWidgets.QTabWidget(self.groupBox_IMG_RESULTANTE)
        self.tabWidget_OPERACIONES.setEnabled(True)
        self.tabWidget_OPERACIONES.setGeometry(QtCore.QRect(20, 20, 691, 351))
        self.tabWidget_OPERACIONES.setToolTip("")
        self.tabWidget_OPERACIONES.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.tabWidget_OPERACIONES.setAutoFillBackground(False)
        self.tabWidget_OPERACIONES.setTabPosition(QtWidgets.QTabWidget.South)
        self.tabWidget_OPERACIONES.setTabShape(QtWidgets.QTabWidget.Triangular)
        self.tabWidget_OPERACIONES.setTabsClosable(False)
        self.tabWidget_OPERACIONES.setMovable(True)
        self.tabWidget_OPERACIONES.setTabBarAutoHide(False)
        self.tabWidget_OPERACIONES.setObjectName("tabWidget_OPERACIONES")
        self.tab_IMAGEN_A = QtWidgets.QWidget()
        self.tab_IMAGEN_A.setObjectName("tab_IMAGEN_A")
        self.photo_ImagenA = QtWidgets.QLabel(self.tab_IMAGEN_A)
        self.photo_ImagenA.setGeometry(QtCore.QRect(10, 10, 661, 311))
        self.photo_ImagenA.setText("")
        self.photo_ImagenA.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_ImagenA.setScaledContents(True)
        self.photo_ImagenA.setObjectName("photo_ImagenA")
        self.tabWidget_OPERACIONES.addTab(self.tab_IMAGEN_A, "")
        self.tab_IMAGEN_B = QtWidgets.QWidget()
        self.tab_IMAGEN_B.setObjectName("tab_IMAGEN_B")
        self.photo_ImagenB = QtWidgets.QLabel(self.tab_IMAGEN_B)
        self.photo_ImagenB.setGeometry(QtCore.QRect(10, 10, 661, 311))
        self.photo_ImagenB.setText("")
        self.photo_ImagenB.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_ImagenB.setScaledContents(True)
        self.photo_ImagenB.setObjectName("photo_ImagenB")
        self.tabWidget_OPERACIONES.addTab(self.tab_IMAGEN_B, "")
        self.tab_RESULTADO = QtWidgets.QWidget()
        self.tab_RESULTADO.setObjectName("tab_RESULTADO")
        self.photo_RESULTADO = QtWidgets.QLabel(self.tab_RESULTADO)
        self.photo_RESULTADO.setGeometry(QtCore.QRect(10, 10, 661, 311))
        self.photo_RESULTADO.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.photo_RESULTADO.setText("")
        self.photo_RESULTADO.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTADO.setScaledContents(True)
        self.photo_RESULTADO.setObjectName("photo_RESULTADO")
        self.tabWidget_OPERACIONES.addTab(self.tab_RESULTADO, "")
        VENTANA_OPERACIONES.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(VENTANA_OPERACIONES)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 750, 21))
        self.menubar.setObjectName("menubar")
        VENTANA_OPERACIONES.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(VENTANA_OPERACIONES)
        self.statusbar.setObjectName("statusbar")
        VENTANA_OPERACIONES.setStatusBar(self.statusbar)

        self.retranslateUi(VENTANA_OPERACIONES)
        self.stackedWidget_SUMAS_RESTAS.setCurrentIndex(0)
        self.tabWidget_OPERACIONES.setCurrentIndex(2)
        
        self.btn_ABRIRIMAGEN_A.clicked.connect(  self.show_IMG_1 )
        self.btn_ABRIRIMAGEN_B.clicked.connect(  self.show_IMG_2 )
        # self.btn_OPERAR.clicked.connect(  self.show_IMG_RESULTANTE )
        self.btn_OPERAR.clicked.connect(  self.obtenerALPHA )
        self.btn_OPERAR.clicked.connect(  self.obtenerBETHA )
        
        self.radioButton_VENTANA1_SUMA.clicked.connect( lambda: self.stackedWidget_SUMAS_RESTAS.setCurrentIndex(0) )
        self.radioButton_VENTANA2_RESTA.clicked.connect( lambda: self.stackedWidget_SUMAS_RESTAS.setCurrentIndex(1) )
        self.radioButton_ADD.clicked.connect( lambda: self.groupBox_ALPHA_BETHA.setEnabled(False))
        self.radioButton_ADDWEIGHTED.clicked.connect( lambda: self.groupBox_ALPHA_BETHA.setEnabled(True))
        self.radioButton_ABSDIFF.clicked.connect( lambda: self.groupBox_ALPHA_BETHA.setEnabled(False))
        self.radioButton_SUBTRACT.clicked.connect( lambda: self.groupBox_ALPHA_BETHA.setEnabled(False))
        self.radioButton_VENTANA2_RESTA.clicked.connect( lambda: self.groupBox_ALPHA_BETHA.setEnabled(False))
        
        QtCore.QMetaObject.connectSlotsByName(VENTANA_OPERACIONES)

    def retranslateUi(self, VENTANA_OPERACIONES):
        _translate = QtCore.QCoreApplication.translate
        VENTANA_OPERACIONES.setWindowTitle(_translate("VENTANA_OPERACIONES", "Operaciones"))
        self.groupBox_ACCIONES.setTitle(_translate("VENTANA_OPERACIONES", "Acciones"))
        self.btn_ABRIRIMAGEN_A.setText(_translate("VENTANA_OPERACIONES", "Imagen A"))
        self.btn_ABRIRIMAGEN_A.setShortcut(_translate("VENTANA_OPERACIONES", "Ctrl+A"))
        self.btn_OPERAR.setText(_translate("VENTANA_OPERACIONES", "Operar"))
        self.btn_OPERAR.setShortcut(_translate("VENTANA_OPERACIONES", "Ctrl+F"))
        self.radioButton_ADD.setText(_translate("VENTANA_OPERACIONES", "Add"))
        self.radioButton_ADDWEIGHTED.setText(_translate("VENTANA_OPERACIONES", "AddWeighted"))
        self.radioButton_ABSDIFF.setText(_translate("VENTANA_OPERACIONES", "Absdiff"))
        self.radioButton_SUBTRACT.setText(_translate("VENTANA_OPERACIONES", "Subtract"))
        self.radioButton_VENTANA2_RESTA.setText(_translate("VENTANA_OPERACIONES", "Subtracción"))
        self.radioButton_VENTANA1_SUMA.setText(_translate("VENTANA_OPERACIONES", "Adición"))
        self.btn_ABRIRIMAGEN_B.setText(_translate("VENTANA_OPERACIONES", "Imagen B"))
        self.btn_ABRIRIMAGEN_B.setShortcut(_translate("VENTANA_OPERACIONES", "Ctrl+A"))
        self.label_ALPHA.setText(_translate("VENTANA_OPERACIONES", "α"))
        self.label_BETHA.setText(_translate("VENTANA_OPERACIONES", "β"))
        self.groupBox_IMG_RESULTANTE.setTitle(_translate("VENTANA_OPERACIONES", "Imagen Procesada"))
        self.tabWidget_OPERACIONES.setTabText(self.tabWidget_OPERACIONES.indexOf(self.tab_IMAGEN_A), _translate("VENTANA_OPERACIONES", "Imagen A"))
        self.tabWidget_OPERACIONES.setTabText(self.tabWidget_OPERACIONES.indexOf(self.tab_IMAGEN_B), _translate("VENTANA_OPERACIONES", "Imagen B"))
        self.tabWidget_OPERACIONES.setTabText(self.tabWidget_OPERACIONES.indexOf(self.tab_RESULTADO), _translate("VENTANA_OPERACIONES", "Resultado"))
    
    def show_IMG_1 (self):
        global fileName_IMG_1
        
        fileName_IMG_1 = self.abrir_imagen()
        self.photo_ImagenA.setPixmap(QtGui.QPixmap(fileName_IMG_1))
    
    def show_IMG_2 (self):
        global fileName_IMG_2
        
        fileName_IMG_2 = self.abrir_imagen()
        self.photo_ImagenB.setPixmap(QtGui.QPixmap(fileName_IMG_2))
    
    def show_IMG_RESULTANTE(self):
        try:
            if self.radioButton_VENTANA1_SUMA.isChecked()==True: 
                if self.radioButton_ADD.isChecked()==True:
                    print ('ADD')
                    nameImg = oper2.getAdd(fileName_IMG_1, fileName_IMG_2)
                    self.photo_RESULTADO.setPixmap(QtGui.QPixmap( nameImg ))
                    
                    
                elif self.radioButton_ADDWEIGHTED.isChecked()==True:
                    print ('ADDWEIGHTED')
                    nameImg = oper2.getaddWeighted(fileName_IMG_1, self.obtenerALPHA(), fileName_IMG_2, self.obtenerBETHA(), 0 )
                    self.photo_RESULTADO.setPixmap(QtGui.QPixmap( nameImg ))
                else:
                    print ('No hay nada mas que hacer1')
                    
            elif self.radioButton_VENTANA2_RESTA.isChecked()==True:
                
                if self.radioButton_ABSDIFF.isChecked()==True:
                    print ('ABSDIF')
                    nameImg = oper2.getabsdiff(fileName_IMG_1, fileName_IMG_2)
                    self.photo_RESULTADO.setPixmap(QtGui.QPixmap( nameImg ))
                                        
                elif self.radioButton_SUBTRACT.isChecked()==True:
                    print ('SUBTRACT')
                    nameImg = oper2.getSubtract(fileName_IMG_1, fileName_IMG_2)
                    self.photo_RESULTADO.setPixmap(QtGui.QPixmap( nameImg ))
                else:
                    print ('No hay nada mas que hacer1')
                    
            else:
                print ('No hay nada mas que hacer2')
            
        except:
            self.clickMethod()
                
    def clickMethod(self):
        q = QMessageBox(QMessageBox.Warning, extentionImg.typeAlert, extentionImg.typeMessage)
        q.setStandardButtons(QMessageBox.Ok);
        q.exec_()
            
    def abrir_imagen(self):
        title = extentionImg.titulo
        qfd = QFileDialog()
        path = paths.PATH
        filter = extentionImg.filtroIMG
        f, _ = QFileDialog.getOpenFileName(qfd, title, path, filter)
        return str(f)
    
    def obtenerALPHA(self):
        return round( self.doubleSpinBox_ALPHA.value(), 1 )
        
        
    def obtenerBETHA(self):
        return round( self.doubleSpinBox_BETHA.value(), 1 )

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    VENTANA_OPERACIONES = QtWidgets.QMainWindow()
    ui = Ui_VENTANA_OPERACIONES()
    ui.setupUi(VENTANA_OPERACIONES)
    VENTANA_OPERACIONES.show()
    sys.exit(app.exec_())

