# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventana_NDVI.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QPushButton
from NDVI_files import ndvi as ndvi1
from NDVI_files import rutas_D_Archivos as paths
from TipoIMG import configuracion as extentionImg

class Ui_Ventana_NDVI(object):
    def setupUi(self, Ventana_NDVI):
        Ventana_NDVI.setObjectName("Ventana_NDVI")
        Ventana_NDVI.resize(790, 420)
        Ventana_NDVI.setMinimumSize(QtCore.QSize(790, 420))
        Ventana_NDVI.setMaximumSize(QtCore.QSize(790, 420))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        Ventana_NDVI.setFont(font)
        Ventana_NDVI.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        Ventana_NDVI.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.centralwidget = QtWidgets.QWidget(Ventana_NDVI)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 141, 141))
        self.groupBox.setObjectName("groupBox")
        self.NIR = QtWidgets.QPushButton(self.groupBox)
        self.NIR.setGeometry(QtCore.QRect(14, 20, 111, 30))
        self.NIR.setObjectName("NIR")
        self.RED = QtWidgets.QPushButton(self.groupBox)
        self.RED.setGeometry(QtCore.QRect(14, 60, 111, 30))
        self.RED.setObjectName("RED")
        self.NDVI_btn = QtWidgets.QPushButton(self.groupBox)
        self.NDVI_btn.setGeometry(QtCore.QRect(14, 100, 111, 30))
        self.NDVI_btn.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
        self.NDVI_btn.setObjectName("NDVI_btn")
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(160, 10, 621, 371))
        self.groupBox_2.setObjectName("groupBox_2")
        self.photo = QtWidgets.QLabel(self.groupBox_2)
        self.photo.setGeometry(QtCore.QRect(10, 20, 601, 341))
        self.photo.setText("")
        # self.photo.setPixmap(QtGui.QPixmap("img/NIR.TIF"))
        self.photo.setScaledContents(True)
        self.photo.setObjectName("photo")
        self.groupBox_3 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_3.setGeometry(QtCore.QRect(10, 160, 141, 111))
        self.groupBox_3.setObjectName("groupBox_3")
        self.label = QtWidgets.QLabel(self.groupBox_3)
        self.label.setGeometry(QtCore.QRect(10, 20, 121, 81))
        self.label.setText("")
        self.label.setScaledContents(True)
        self.label.setObjectName("label")
        self.groupBox_4 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_4.setGeometry(QtCore.QRect(10, 270, 141, 111))
        self.groupBox_4.setObjectName("groupBox_4")
        self.label_2 = QtWidgets.QLabel(self.groupBox_4)
        self.label_2.setGeometry(QtCore.QRect(10, 20, 121, 81))
        self.label_2.setText("")
        self.label_2.setScaledContents(True)
        self.label_2.setObjectName("label_2")
        Ventana_NDVI.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Ventana_NDVI)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 790, 23))
        self.menubar.setObjectName("menubar")
        Ventana_NDVI.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(Ventana_NDVI)
        self.statusbar.setObjectName("statusbar")
        Ventana_NDVI.setStatusBar(self.statusbar)

        self.retranslateUi(Ventana_NDVI)
        self.NIR.clicked['bool'].connect(self.label.show)
        self.RED.clicked['bool'].connect(self.label_2.show)
        QtCore.QMetaObject.connectSlotsByName(Ventana_NDVI)
        
        self.RED.clicked.connect(self.show_RED)
        self.NIR.clicked.connect(self.show_NIR)
        self.NDVI_btn.clicked.connect(self.mostrarNDVI)

    def retranslateUi(self, Ventana_NDVI):
        _translate = QtCore.QCoreApplication.translate
        Ventana_NDVI.setWindowTitle(_translate("Ventana_NDVI", "NDVI"))
        self.groupBox.setTitle(_translate("Ventana_NDVI", "Acciones"))
        self.NIR.setText(_translate("Ventana_NDVI", "NIR"))
        self.RED.setText(_translate("Ventana_NDVI", "RED"))
        self.NDVI_btn.setText(_translate("Ventana_NDVI", "Generar NDVI"))
        self.groupBox_2.setTitle(_translate("Ventana_NDVI", "Procesamiento del NDVI"))
        self.groupBox_3.setTitle(_translate("Ventana_NDVI", "NIR"))
        self.groupBox_4.setTitle(_translate("Ventana_NDVI", "RED"))
    
    def show_NIR (self):
        global fileName_NIR
        
        fileName_NIR = self.abrir_imagen()
        self.photo.setPixmap(QtGui.QPixmap(fileName_NIR))
        self.label.setPixmap(QtGui.QPixmap(fileName_NIR))
        
    def show_RED (self):
        global fileName_RED
        
        fileName_RED = self.abrir_imagen()
        self.photo.setPixmap(QtGui.QPixmap(fileName_RED))
        self.label_2.setPixmap(QtGui.QPixmap(fileName_RED))
        
    def mostrarNDVI (self):
        try:    
            thisNDVI = ndvi1.mostrarImgDelNDVI (fileName_NIR, fileName_RED)
            self.photo.setPixmap(QtGui.QPixmap(thisNDVI))
        except:
            self.clickMethod()
                
    def clickMethod(self):
        q = QMessageBox(QMessageBox.Warning, extentionImg.typeAlert, extentionImg.typeMessage)
        q.setStandardButtons(QMessageBox.Ok);
        # i = QIcon()
        # i.addPixmap(QPixmap("..."), 0)
        # q.setWindowIcon(i)
        q.exec_()
        
            
    def abrir_imagen(self):
        title = extentionImg.titulo
        qfd = QFileDialog()
        path = paths.PATH
        filter = extentionImg.filtroIMG
        f, _ = QFileDialog.getOpenFileName(qfd, title, path, filter)
        return str(f)
    
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Ventana_NDVI = QtWidgets.QMainWindow()
    ui = Ui_Ventana_NDVI()
    ui.setupUi(Ventana_NDVI)
    Ventana_NDVI.show()
    sys.exit(app.exec_())

