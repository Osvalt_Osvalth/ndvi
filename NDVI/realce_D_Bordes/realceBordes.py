# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 21:00:24 2020

@author: music
"""
import cv2

from NDVI_files import leerIMGs_TIF as ReadWrittenIMGs
from NDVI_files import rutas_D_Archivos as myPath

def getPathBynaryByName(nameImage , histo):
    guardarRutaCategoria = myPath.getPathByCategory(nameImage, histo)
    return guardarRutaCategoria

def getEdgeEnhancement ( ImG_Original, ImG_Filter ):
    imgORI = cv2.imread( ImG_Original )
    imgFIL = cv2.imread( ImG_Filter )
    while(1):
        nameImg, nameHisto = getPathBynaryByName('REALCE_BORDES', histo = 'HISTOGRAMA')
        resultado = cv2.absdiff ( imgORI, imgFIL )
        ReadWrittenIMGs.writteImgBlack( nameImg, resultado )
        return nameImg
    
