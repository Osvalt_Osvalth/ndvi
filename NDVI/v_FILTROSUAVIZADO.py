# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventana_MedianBlur2.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QPushButton
from NDVI_files import rutas_D_Archivos as paths
from median_Blur import median_Blur as TB
from TipoIMG import configuracion as extentionImg

class Ui_Ventana_MEDIANBLUR(object):
    def setupUi(self, Ventana_MEDIANBLUR):
        Ventana_MEDIANBLUR.setObjectName("Ventana_MEDIANBLUR")
        Ventana_MEDIANBLUR.resize(800, 570)
        Ventana_MEDIANBLUR.setMinimumSize(QtCore.QSize(800, 570))
        Ventana_MEDIANBLUR.setMaximumSize(QtCore.QSize(800, 570))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        Ventana_MEDIANBLUR.setFont(font)
        Ventana_MEDIANBLUR.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        Ventana_MEDIANBLUR.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.centralwidget = QtWidgets.QWidget(Ventana_MEDIANBLUR)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox_Img_ORIGINAL = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_Img_ORIGINAL.setGeometry(QtCore.QRect(260, 0, 261, 171))
        self.groupBox_Img_ORIGINAL.setObjectName("groupBox_Img_ORIGINAL")
        self.photo_ORIGINAL = QtWidgets.QLabel(self.groupBox_Img_ORIGINAL)
        self.photo_ORIGINAL.setGeometry(QtCore.QRect(10, 25, 241, 131))
        self.photo_ORIGINAL.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.photo_ORIGINAL.setText("")
        self.photo_ORIGINAL.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_ORIGINAL.setScaledContents(True)
        self.photo_ORIGINAL.setAlignment(QtCore.Qt.AlignCenter)
        self.photo_ORIGINAL.setIndent(-1)
        self.photo_ORIGINAL.setObjectName("photo_ORIGINAL")
        self.groupBox_Acciones = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_Acciones.setGeometry(QtCore.QRect(0, 0, 251, 171))
        self.groupBox_Acciones.setObjectName("groupBox_Acciones")
        self.horizontalSlider = QtWidgets.QSlider(self.groupBox_Acciones)
        self.horizontalSlider.setGeometry(QtCore.QRect(20, 110, 221, 41))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.horizontalSlider.setFont(font)
        self.horizontalSlider.setMinimum(1)
        self.horizontalSlider.setMaximum(255)
        self.horizontalSlider.setTracking(False)
        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setObjectName("horizontalSlider")
        self.btn_HISTOGRAMA = QtWidgets.QPushButton(self.groupBox_Acciones)
        self.btn_HISTOGRAMA.setGeometry(QtCore.QRect(130, 20, 111, 30))
        self.btn_HISTOGRAMA.setObjectName("btn_HISTOGRAMA")
        self.spinBox_mEDIANbLUR = QtWidgets.QSpinBox(self.groupBox_Acciones)
        self.spinBox_mEDIANbLUR.setGeometry(QtCore.QRect(132, 60, 108, 30))
        self.spinBox_mEDIANbLUR.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.spinBox_mEDIANbLUR.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.spinBox_mEDIANbLUR.setKeyboardTracking(False)
        self.spinBox_mEDIANbLUR.setMinimum(1)
        self.spinBox_mEDIANbLUR.setMaximum(255)
        self.spinBox_mEDIANbLUR.setSingleStep(1)
        self.spinBox_mEDIANbLUR.setProperty("value", 1)
        self.spinBox_mEDIANbLUR.setObjectName("spinBox_mEDIANbLUR")
        self.btn_ABRIR = QtWidgets.QPushButton(self.groupBox_Acciones)
        self.btn_ABRIR.setGeometry(QtCore.QRect(14, 20, 111, 30))
        self.btn_ABRIR.setObjectName("btn_ABRIR")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(530, 0, 251, 171))
        self.groupBox.setObjectName("groupBox")
        self.photo_HISTOGRAMA = QtWidgets.QLabel(self.groupBox)
        self.photo_HISTOGRAMA.setGeometry(QtCore.QRect(10, 20, 231, 141))
        self.photo_HISTOGRAMA.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.photo_HISTOGRAMA.setText("")
        self.photo_HISTOGRAMA.setPixmap(QtGui.QPixmap("../imgs/IMG_1593142839540_HISTOGRAMA.TIF"))
        self.photo_HISTOGRAMA.setScaledContents(True)
        self.photo_HISTOGRAMA.setObjectName("photo_HISTOGRAMA")
        self.groupBox_img_RESULTADO = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_img_RESULTADO.setGeometry(QtCore.QRect(0, 170, 781, 381))
        self.groupBox_img_RESULTADO.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.groupBox_img_RESULTADO.setObjectName("groupBox_img_RESULTADO")
        self.photo_RESULTANTE = QtWidgets.QLabel(self.groupBox_img_RESULTADO)
        self.photo_RESULTANTE.setGeometry(QtCore.QRect(40, 20, 711, 351))
        self.photo_RESULTANTE.setMinimumSize(QtCore.QSize(711, 0))
        self.photo_RESULTANTE.setSizeIncrement(QtCore.QSize(2, 0))
        self.photo_RESULTANTE.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.photo_RESULTANTE.setFocusPolicy(QtCore.Qt.NoFocus)
        self.photo_RESULTANTE.setText("")
        self.photo_RESULTANTE.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTANTE.setScaledContents(True)
        self.photo_RESULTANTE.setAlignment(QtCore.Qt.AlignCenter)
        self.photo_RESULTANTE.setWordWrap(False)
        self.photo_RESULTANTE.setObjectName("photo_RESULTANTE")
        Ventana_MEDIANBLUR.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Ventana_MEDIANBLUR)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        Ventana_MEDIANBLUR.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(Ventana_MEDIANBLUR)
        self.statusbar.setObjectName("statusbar")
        Ventana_MEDIANBLUR.setStatusBar(self.statusbar)

        self.retranslateUi(Ventana_MEDIANBLUR)
        
        self.btn_ABRIR.clicked.connect(self.show_IMG_1)
        self.horizontalSlider.valueChanged['int'].connect(self.show_IMG_2)
        self.btn_HISTOGRAMA.clicked['bool'].connect(self.mostrarHistogramaDeImagen)
        self.horizontalSlider.valueChanged['int'].connect(self.spinBox_mEDIANbLUR.setValue)
        self.horizontalSlider.valueChanged['int'].connect(self.mostrarHistogramaDeImagen)
        self.spinBox_mEDIANbLUR.valueChanged['int'].connect(self.horizontalSlider.setValue)
        
        QtCore.QMetaObject.connectSlotsByName(Ventana_MEDIANBLUR)

    def retranslateUi(self, Ventana_MEDIANBLUR):
        _translate = QtCore.QCoreApplication.translate
        Ventana_MEDIANBLUR.setWindowTitle(_translate("Ventana_MEDIANBLUR", "Filtro Median Blur"))
        self.groupBox_Img_ORIGINAL.setTitle(_translate("Ventana_MEDIANBLUR", "Imagen original"))
        self.groupBox_Acciones.setTitle(_translate("Ventana_MEDIANBLUR", "Acciones"))
        self.btn_HISTOGRAMA.setText(_translate("Ventana_MEDIANBLUR", "Histograma"))
        self.btn_ABRIR.setText(_translate("Ventana_MEDIANBLUR", "Abrir"))
        self.groupBox.setTitle(_translate("Ventana_MEDIANBLUR", "Histograma"))
        self.groupBox_img_RESULTADO.setTitle(_translate("Ventana_MEDIANBLUR", "Imagen procesada"))
        self.photo_RESULTANTE.setWhatsThis(_translate("Ventana_MEDIANBLUR", "<html><head/><body><p align=\"center\"><span style=\" font-size:72pt;\">1</span></p></body></html>"))
    
    '''---------------   Definiciones propias o métodos a usar   -----------'''
    def show_IMG_1 (self):
        global fileName_IMG
        
        fileName_IMG = self.abrir_imagen()
        self.photo_ORIGINAL.setPixmap(QtGui.QPixmap(fileName_IMG))
        
    def show_IMG_2(self):
        global thisIMAGE, HistogramaPath
        try:
            thisIMAGE, HistogramaPath  = TB.calcularMedianBlur ( fileName_IMG, self.getValueH_1() )
            # print('xdxd '+HistogramaPath)
            self.photo_RESULTANTE.setPixmap(QtGui.QPixmap(thisIMAGE))
        except:
            self.clickMethod()
    
    def mostrarHistogramaDeImagen(self):
        # try:
            # hISTO = getHistogram(thisIMAGE, HistogramaPath)
            # self.photo_HISTOGRAMA.setPixmap(QtGui.QPixmap(hISTO))
            self.photo_HISTOGRAMA.setPixmap(QtGui.QPixmap(HistogramaPath))
        # except:
        #     self.clickMethod()
                
    def clickMethod(self):
        q = QMessageBox(QMessageBox.Warning, extentionImg.typeAlert, extentionImg.typeMessage)
        q.setStandardButtons(QMessageBox.Ok)
        q.exec_()
    
    
    def abrir_imagen(self):
        title = extentionImg.titulo
        qfd = QFileDialog()
        path = paths.PATH
        filter = extentionImg.filtroIMG
        f, _ = QFileDialog.getOpenFileName(qfd, title, path, filter)
        return str(f)
    
    def getValueH_1(self):
        return self.horizontalSlider.value()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Ventana_MEDIANBLUR = QtWidgets.QMainWindow()
    ui = Ui_Ventana_MEDIANBLUR()
    ui.setupUi(Ventana_MEDIANBLUR)
    Ventana_MEDIANBLUR.show()
    sys.exit(app.exec_())

