# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventana_COLABORACION.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_VENT_COLABORACION(object):
    def setupUi(self, VENT_COLABORACION):
        VENT_COLABORACION.setObjectName("VENT_COLABORACION")
        VENT_COLABORACION.resize(328, 168)
        VENT_COLABORACION.setMinimumSize(QtCore.QSize(328, 168))
        VENT_COLABORACION.setMaximumSize(QtCore.QSize(328, 168))
        VENT_COLABORACION.setSizeIncrement(QtCore.QSize(0, 0))
        self.centralwidget = QtWidgets.QWidget(VENT_COLABORACION)
        self.centralwidget.setObjectName("centralwidget")
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(10, 0, 311, 141))
        self.textEdit.setMinimumSize(QtCore.QSize(300, 141))
        self.textEdit.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.textEdit.setUndoRedoEnabled(True)
        self.textEdit.setReadOnly(True)
        self.textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.textEdit.setObjectName("textEdit")
        VENT_COLABORACION.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(VENT_COLABORACION)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 328, 21))
        self.menubar.setObjectName("menubar")
        VENT_COLABORACION.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(VENT_COLABORACION)
        self.statusbar.setObjectName("statusbar")
        VENT_COLABORACION.setStatusBar(self.statusbar)

        self.retranslateUi(VENT_COLABORACION)
        QtCore.QMetaObject.connectSlotsByName(VENT_COLABORACION)

    def retranslateUi(self, VENT_COLABORACION):
        _translate = QtCore.QCoreApplication.translate
        VENT_COLABORACION.setWindowTitle(_translate("VENT_COLABORACION", "Colaboración"))
        self.textEdit.setHtml(_translate("VENT_COLABORACION", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:11pt; font-weight:600;\">Mejoramiento de imágenes</span></p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt; font-weight:600;\"><br /></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:11pt;\">Proyecto realizado por alumnos de </span></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:11pt;\">Ingeniería en Telemática del nivel superior </span></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:11pt;\">de la </span><span style=\" font-size:11pt; font-weight:600;\">UPIITA - IPN</span><span style=\" font-size:11pt;\"> en colaboación con el </span></p>\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:11pt;\">Ingenio Azúcarero</span><span style=\" font-size:11pt; font-weight:600;\"> El potrero</span></p>\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt; font-weight:600;\"><br /></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    VENT_COLABORACION = QtWidgets.QMainWindow()
    ui = Ui_VENT_COLABORACION()
    ui.setupUi(VENT_COLABORACION)
    VENT_COLABORACION.show()
    sys.exit(app.exec_())

