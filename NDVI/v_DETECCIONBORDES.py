# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventana_Filtros_SOBEL_LAPLACE_CANNY.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QPushButton
from NDVI_files import rutas_D_Archivos as paths
from filtros import deteccion_D_Bordes as filtros_SLC
from TipoIMG import configuracion as extentionImg

class Ui_Ventana_DETECCIONBORDE3(object):
    def setupUi(self, Ventana_DETECCIONBORDE3):
        Ventana_DETECCIONBORDE3.setObjectName("Ventana_DETECCIONBORDE3")
        Ventana_DETECCIONBORDE3.resize(800, 565)
        Ventana_DETECCIONBORDE3.setMinimumSize(QtCore.QSize(800, 565))
        Ventana_DETECCIONBORDE3.setMaximumSize(QtCore.QSize(800, 565))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        Ventana_DETECCIONBORDE3.setFont(font)
        Ventana_DETECCIONBORDE3.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        Ventana_DETECCIONBORDE3.setMouseTracking(False)
        Ventana_DETECCIONBORDE3.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.centralwidget = QtWidgets.QWidget(Ventana_DETECCIONBORDE3)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox_ACCIONES = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_ACCIONES.setGeometry(QtCore.QRect(10, 0, 451, 171))
        self.groupBox_ACCIONES.setObjectName("groupBox_ACCIONES")
        self.btn_ABRIR = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_ABRIR.setGeometry(QtCore.QRect(20, 20, 111, 30))
        self.btn_ABRIR.setObjectName("btn_ABRIR")
        self.btn_FILTRAR = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_FILTRAR.setGeometry(QtCore.QRect(20, 130, 111, 30))
        self.btn_FILTRAR.setObjectName("btn_FILTRAR")
        self.stackedWidget_FILTROS = QtWidgets.QStackedWidget(self.groupBox_ACCIONES)
        self.stackedWidget_FILTROS.setEnabled(True)
        self.stackedWidget_FILTROS.setGeometry(QtCore.QRect(149, 20, 291, 141))
        self.stackedWidget_FILTROS.setFrameShape(QtWidgets.QFrame.Box)
        self.stackedWidget_FILTROS.setFrameShadow(QtWidgets.QFrame.Raised)
        self.stackedWidget_FILTROS.setObjectName("stackedWidget_FILTROS")
        self.PAGE_SOBEL_LAPLACE = QtWidgets.QWidget()
        self.PAGE_SOBEL_LAPLACE.setObjectName("PAGE_SOBEL_LAPLACE")
        self.groupBox = QtWidgets.QGroupBox(self.PAGE_SOBEL_LAPLACE)
        self.groupBox.setGeometry(QtCore.QRect(5, 40, 281, 91))
        self.groupBox.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.comboBox_KERNELSIZE = QtWidgets.QComboBox(self.groupBox)
        self.comboBox_KERNELSIZE.setGeometry(QtCore.QRect(150, 50, 121, 30))
        self.comboBox_KERNELSIZE.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.comboBox_KERNELSIZE.setMaxVisibleItems(10)
        self.comboBox_KERNELSIZE.setFrame(False)
        self.comboBox_KERNELSIZE.setObjectName("comboBox_KERNELSIZE")
        self.comboBox_KERNELSIZE.addItem("")
        self.comboBox_KERNELSIZE.addItem("")
        self.comboBox_KERNELSIZE.addItem("")
        self.label_preFILTRO = QtWidgets.QLabel(self.groupBox)
        self.label_preFILTRO.setGeometry(QtCore.QRect(14, 10, 111, 30))
        self.label_preFILTRO.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_preFILTRO.setObjectName("label_preFILTRO")
        self.label_KERNEL = QtWidgets.QLabel(self.groupBox)
        self.label_KERNEL.setGeometry(QtCore.QRect(10, 50, 121, 30))
        self.label_KERNEL.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_KERNEL.setObjectName("label_KERNEL")
        self.comboBox_FILTRO_GAUSS12 = QtWidgets.QComboBox(self.groupBox)
        self.comboBox_FILTRO_GAUSS12.setGeometry(QtCore.QRect(150, 10, 125, 30))
        self.comboBox_FILTRO_GAUSS12.setObjectName("comboBox_FILTRO_GAUSS12")
        self.comboBox_FILTRO_GAUSS12.addItem("")
        self.comboBox_FILTRO_GAUSS12.addItem("")
        self.comboBox_FILTRO_GAUSS12.addItem("")
        self.radioButton_SOBEL = QtWidgets.QRadioButton(self.PAGE_SOBEL_LAPLACE)
        self.radioButton_SOBEL.setGeometry(QtCore.QRect(40, 10, 82, 17))
        self.radioButton_SOBEL.setChecked(True)
        self.radioButton_SOBEL.setObjectName("radioButton_SOBEL")
        self.radioButton_LAPLACE = QtWidgets.QRadioButton(self.PAGE_SOBEL_LAPLACE)
        self.radioButton_LAPLACE.setGeometry(QtCore.QRect(150, 10, 82, 17))
        self.radioButton_LAPLACE.setObjectName("radioButton_LAPLACE")
        self.stackedWidget_FILTROS.addWidget(self.PAGE_SOBEL_LAPLACE)
        self.page_CANNY = QtWidgets.QWidget()
        self.page_CANNY.setObjectName("page_CANNY")
        self.label_CANNY = QtWidgets.QLabel(self.page_CANNY)
        self.label_CANNY.setGeometry(QtCore.QRect(125, 2, 43, 21))
        self.label_CANNY.setObjectName("label_CANNY")
        self.groupBox_2 = QtWidgets.QGroupBox(self.page_CANNY)
        self.groupBox_2.setGeometry(QtCore.QRect(4, 23, 281, 111))
        self.groupBox_2.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.groupBox_2.setTitle("")
        self.groupBox_2.setObjectName("groupBox_2")
        self.comboBox_KERNELSIZE_CANNY = QtWidgets.QComboBox(self.groupBox_2)
        self.comboBox_KERNELSIZE_CANNY.setGeometry(QtCore.QRect(150, 30, 41, 30))
        self.comboBox_KERNELSIZE_CANNY.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.comboBox_KERNELSIZE_CANNY.setCurrentText("")
        self.comboBox_KERNELSIZE_CANNY.setMaxVisibleItems(10)
        self.comboBox_KERNELSIZE_CANNY.setFrame(False)
        self.comboBox_KERNELSIZE_CANNY.setObjectName("comboBox_KERNELSIZE_CANNY")
        self.comboBox_KERNELSIZE_CANNY.addItem("")
        self.comboBox_KERNELSIZE_CANNY.addItem("")
        self.comboBox_KERNELSIZE_CANNY.addItem("")
        self.comboBox_KERNELSIZE_CANNY.addItem("")
        self.label_preFILTRO_CANNY = QtWidgets.QLabel(self.groupBox_2)
        self.label_preFILTRO_CANNY.setGeometry(QtCore.QRect(14, 0, 111, 30))
        self.label_preFILTRO_CANNY.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_preFILTRO_CANNY.setObjectName("label_preFILTRO_CANNY")
        self.label_KERNEL_CANNY = QtWidgets.QLabel(self.groupBox_2)
        self.label_KERNEL_CANNY.setGeometry(QtCore.QRect(10, 30, 121, 30))
        self.label_KERNEL_CANNY.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_KERNEL_CANNY.setObjectName("label_KERNEL_CANNY")
        self.label_GASSIANBLUR_CANNY = QtWidgets.QLabel(self.groupBox_2)
        self.label_GASSIANBLUR_CANNY.setGeometry(QtCore.QRect(150, 0, 91, 30))
        self.label_GASSIANBLUR_CANNY.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_GASSIANBLUR_CANNY.setObjectName("label_GASSIANBLUR_CANNY")
        self.label_UMBRAL_MIN_CANNY = QtWidgets.QLabel(self.groupBox_2)
        self.label_UMBRAL_MIN_CANNY.setGeometry(QtCore.QRect(0, 70, 81, 30))
        self.label_UMBRAL_MIN_CANNY.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_UMBRAL_MIN_CANNY.setObjectName("label_UMBRAL_MIN_CANNY")
        self.comboBox_UMBRAL_MIN_CANNY = QtWidgets.QComboBox(self.groupBox_2)
        self.comboBox_UMBRAL_MIN_CANNY.setGeometry(QtCore.QRect(90, 70, 41, 30))
        self.comboBox_UMBRAL_MIN_CANNY.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.comboBox_UMBRAL_MIN_CANNY.setCurrentText("")
        self.comboBox_UMBRAL_MIN_CANNY.setMaxVisibleItems(12)
        self.comboBox_UMBRAL_MIN_CANNY.setFrame(False)
        self.comboBox_UMBRAL_MIN_CANNY.setObjectName("comboBox_UMBRAL_MIN_CANNY")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MIN_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY = QtWidgets.QComboBox(self.groupBox_2)
        self.comboBox_UMBRAL_MAX_CANNY.setGeometry(QtCore.QRect(230, 70, 41, 30))
        self.comboBox_UMBRAL_MAX_CANNY.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.comboBox_UMBRAL_MAX_CANNY.setCurrentText("")
        self.comboBox_UMBRAL_MAX_CANNY.setMaxVisibleItems(12)
        self.comboBox_UMBRAL_MAX_CANNY.setFrame(False)
        self.comboBox_UMBRAL_MAX_CANNY.setObjectName("comboBox_UMBRAL_MAX_CANNY")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.comboBox_UMBRAL_MAX_CANNY.addItem("")
        self.label_UMBRAL_MAX_CANNY = QtWidgets.QLabel(self.groupBox_2)
        self.label_UMBRAL_MAX_CANNY.setGeometry(QtCore.QRect(140, 70, 81, 30))
        self.label_UMBRAL_MAX_CANNY.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_UMBRAL_MAX_CANNY.setObjectName("label_UMBRAL_MAX_CANNY")
        self.stackedWidget_FILTROS.addWidget(self.page_CANNY)
        self.groupBox_3 = QtWidgets.QGroupBox(self.groupBox_ACCIONES)
        self.groupBox_3.setGeometry(QtCore.QRect(20, 50, 120, 80))
        self.groupBox_3.setTitle("")
        self.groupBox_3.setObjectName("groupBox_3")
        self.radioButton_VENTANA_B = QtWidgets.QRadioButton(self.groupBox_3)
        self.radioButton_VENTANA_B.setGeometry(QtCore.QRect(0, 40, 111, 30))
        self.radioButton_VENTANA_B.setChecked(False)
        self.radioButton_VENTANA_B.setAutoExclusive(True)
        self.radioButton_VENTANA_B.setObjectName("radioButton_VENTANA_B")
        self.radioButton_VENTANA_A = QtWidgets.QRadioButton(self.groupBox_3)
        self.radioButton_VENTANA_A.setGeometry(QtCore.QRect(0, 10, 111, 30))
        self.radioButton_VENTANA_A.setChecked(True)
        self.radioButton_VENTANA_A.setObjectName("radioButton_VENTANA_A")
        self.groupBox_IMG_ORIGINAL = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_IMG_ORIGINAL.setGeometry(QtCore.QRect(470, 0, 311, 171))
        self.groupBox_IMG_ORIGINAL.setObjectName("groupBox_IMG_ORIGINAL")
        self.photo_ORIGINAL = QtWidgets.QLabel(self.groupBox_IMG_ORIGINAL)
        self.photo_ORIGINAL.setGeometry(QtCore.QRect(40, 20, 242, 141))
        self.photo_ORIGINAL.setText("")
        self.photo_ORIGINAL.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_ORIGINAL.setScaledContents(True)
        self.photo_ORIGINAL.setObjectName("photo_ORIGINAL")
        self.groupBox_IMG_RESULTANTE = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_IMG_RESULTANTE.setGeometry(QtCore.QRect(10, 170, 781, 381))
        self.groupBox_IMG_RESULTANTE.setObjectName("groupBox_IMG_RESULTANTE")
        self.tabWidget_FILTROS = QtWidgets.QTabWidget(self.groupBox_IMG_RESULTANTE)
        self.tabWidget_FILTROS.setEnabled(True)
        self.tabWidget_FILTROS.setGeometry(QtCore.QRect(10, 20, 761, 351))
        self.tabWidget_FILTROS.setToolTip("")
        self.tabWidget_FILTROS.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.tabWidget_FILTROS.setAutoFillBackground(False)
        self.tabWidget_FILTROS.setTabPosition(QtWidgets.QTabWidget.North)
        self.tabWidget_FILTROS.setTabShape(QtWidgets.QTabWidget.Triangular)
        self.tabWidget_FILTROS.setTabsClosable(False)
        self.tabWidget_FILTROS.setMovable(True)
        self.tabWidget_FILTROS.setTabBarAutoHide(False)
        self.tabWidget_FILTROS.setObjectName("tabWidget_FILTROS")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.photo_RESULTANTE_PREFILTRO_GAUSSIAN = QtWidgets.QLabel(self.tab)
        self.photo_RESULTANTE_PREFILTRO_GAUSSIAN.setGeometry(QtCore.QRect(50, 10, 661, 311))
        self.photo_RESULTANTE_PREFILTRO_GAUSSIAN.setText("")
        self.photo_RESULTANTE_PREFILTRO_GAUSSIAN.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTANTE_PREFILTRO_GAUSSIAN.setScaledContents(True)
        self.photo_RESULTANTE_PREFILTRO_GAUSSIAN.setObjectName("photo_RESULTANTE_PREFILTRO_GAUSSIAN")
        self.tabWidget_FILTROS.addTab(self.tab, "")
        self.tab_SOBEL_X = QtWidgets.QWidget()
        self.tab_SOBEL_X.setObjectName("tab_SOBEL_X")
        self.photo_RESULTANTE_SX = QtWidgets.QLabel(self.tab_SOBEL_X)
        self.photo_RESULTANTE_SX.setGeometry(QtCore.QRect(50, 10, 661, 311))
        self.photo_RESULTANTE_SX.setText("")
        self.photo_RESULTANTE_SX.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTANTE_SX.setScaledContents(True)
        self.photo_RESULTANTE_SX.setObjectName("photo_RESULTANTE_SX")
        self.tabWidget_FILTROS.addTab(self.tab_SOBEL_X, "")
        self.tab_SOBEL_Y = QtWidgets.QWidget()
        self.tab_SOBEL_Y.setObjectName("tab_SOBEL_Y")
        self.photo_RESULTANTE_SY = QtWidgets.QLabel(self.tab_SOBEL_Y)
        self.photo_RESULTANTE_SY.setGeometry(QtCore.QRect(50, 10, 661, 311))
        self.photo_RESULTANTE_SY.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.photo_RESULTANTE_SY.setText("")
        self.photo_RESULTANTE_SY.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTANTE_SY.setScaledContents(True)
        self.photo_RESULTANTE_SY.setObjectName("photo_RESULTANTE_SY")
        self.tabWidget_FILTROS.addTab(self.tab_SOBEL_Y, "")
        self.tab_MAGNITUD = QtWidgets.QWidget()
        self.tab_MAGNITUD.setObjectName("tab_MAGNITUD")
        self.photo_RESULTANTE_MAGNITUD = QtWidgets.QLabel(self.tab_MAGNITUD)
        self.photo_RESULTANTE_MAGNITUD.setGeometry(QtCore.QRect(50, 10, 661, 311))
        self.photo_RESULTANTE_MAGNITUD.setText("")
        self.photo_RESULTANTE_MAGNITUD.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTANTE_MAGNITUD.setScaledContents(True)
        self.photo_RESULTANTE_MAGNITUD.setObjectName("photo_RESULTANTE_MAGNITUD")
        self.tabWidget_FILTROS.addTab(self.tab_MAGNITUD, "")
        self.tab_LAPLACIANO = QtWidgets.QWidget()
        self.tab_LAPLACIANO.setObjectName("tab_LAPLACIANO")
        self.photo_RESULTANTE_LAPLACE = QtWidgets.QLabel(self.tab_LAPLACIANO)
        self.photo_RESULTANTE_LAPLACE.setGeometry(QtCore.QRect(50, 10, 661, 311))
        self.photo_RESULTANTE_LAPLACE.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.photo_RESULTANTE_LAPLACE.setText("")
        self.photo_RESULTANTE_LAPLACE.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTANTE_LAPLACE.setScaledContents(True)
        self.photo_RESULTANTE_LAPLACE.setObjectName("photo_RESULTANTE_LAPLACE")
        self.tabWidget_FILTROS.addTab(self.tab_LAPLACIANO, "")
        self.tab_CANNY = QtWidgets.QWidget()
        self.tab_CANNY.setObjectName("tab_CANNY")
        self.photo_RESULTANTE_CANNY = QtWidgets.QLabel(self.tab_CANNY)
        self.photo_RESULTANTE_CANNY.setGeometry(QtCore.QRect(50, 10, 661, 311))
        self.photo_RESULTANTE_CANNY.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.photo_RESULTANTE_CANNY.setText("")
        self.photo_RESULTANTE_CANNY.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTANTE_CANNY.setScaledContents(True)
        self.photo_RESULTANTE_CANNY.setObjectName("photo_RESULTANTE_CANNY")
        self.tabWidget_FILTROS.addTab(self.tab_CANNY, "")
        Ventana_DETECCIONBORDE3.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Ventana_DETECCIONBORDE3)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        Ventana_DETECCIONBORDE3.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(Ventana_DETECCIONBORDE3)
        self.statusbar.setObjectName("statusbar")
        Ventana_DETECCIONBORDE3.setStatusBar(self.statusbar)

        self.retranslateUi(Ventana_DETECCIONBORDE3)
        self.stackedWidget_FILTROS.setCurrentIndex(0)
        self.comboBox_KERNELSIZE.setCurrentIndex(0)
        self.comboBox_FILTRO_GAUSS12.setCurrentIndex(0)
        self.comboBox_KERNELSIZE_CANNY.setCurrentIndex(-1)
        self.comboBox_UMBRAL_MIN_CANNY.setCurrentIndex(-1)
        self.comboBox_UMBRAL_MAX_CANNY.setCurrentIndex(-1)
        self.tabWidget_FILTROS.setCurrentIndex(4)
        self.btn_ABRIR.clicked.connect(  self.show_IMG_1 )
        self.btn_FILTRAR.clicked.connect(  self.show_IMG_2 )
        self.radioButton_VENTANA_A.clicked.connect( lambda: self.stackedWidget_FILTROS.setCurrentIndex(0) )
        self.radioButton_VENTANA_B.clicked.connect( lambda: self.stackedWidget_FILTROS.setCurrentIndex(1) )
        QtCore.QMetaObject.connectSlotsByName(Ventana_DETECCIONBORDE3)

    def retranslateUi(self, Ventana_DETECCIONBORDE3):
        _translate = QtCore.QCoreApplication.translate
        Ventana_DETECCIONBORDE3.setWindowTitle(_translate("Ventana_DETECCIONBORDE3", "Filtros de deteccion de bordes (Sobel - Laplace -Canny)"))
        self.groupBox_ACCIONES.setTitle(_translate("Ventana_DETECCIONBORDE3", "Acciones"))
        self.btn_ABRIR.setText(_translate("Ventana_DETECCIONBORDE3", "Abrir"))
        self.btn_ABRIR.setShortcut(_translate("Ventana_DETECCIONBORDE3", "Ctrl+A"))
        self.btn_FILTRAR.setText(_translate("Ventana_DETECCIONBORDE3", "Filtrar"))
        self.btn_FILTRAR.setShortcut(_translate("Ventana_DETECCIONBORDE3", "Ctrl+F"))
        self.groupBox.setToolTip(_translate("Ventana_DETECCIONBORDE3", "<html><head/><body><p><br/></p></body></html>"))
        self.comboBox_KERNELSIZE.setCurrentText(_translate("Ventana_DETECCIONBORDE3", "3"))
        self.comboBox_KERNELSIZE.setItemText(0, _translate("Ventana_DETECCIONBORDE3", "3"))
        self.comboBox_KERNELSIZE.setItemText(1, _translate("Ventana_DETECCIONBORDE3", "5"))
        self.comboBox_KERNELSIZE.setItemText(2, _translate("Ventana_DETECCIONBORDE3", "7"))
        self.label_preFILTRO.setText(_translate("Ventana_DETECCIONBORDE3", "Prefiltro"))
        self.label_KERNEL.setText(_translate("Ventana_DETECCIONBORDE3", "Tamaño del kernel"))
        self.comboBox_FILTRO_GAUSS12.setCurrentText(_translate("Ventana_DETECCIONBORDE3", "Ninguno"))
        self.comboBox_FILTRO_GAUSS12.setItemText(0, _translate("Ventana_DETECCIONBORDE3", "Ninguno"))
        self.comboBox_FILTRO_GAUSS12.setItemText(1, _translate("Ventana_DETECCIONBORDE3", "Gaussian Ord. 1"))
        self.comboBox_FILTRO_GAUSS12.setItemText(2, _translate("Ventana_DETECCIONBORDE3", "Gaussian Ord. 2"))
        self.radioButton_SOBEL.setText(_translate("Ventana_DETECCIONBORDE3", "Sobel"))
        self.radioButton_LAPLACE.setText(_translate("Ventana_DETECCIONBORDE3", "Laplace"))
        self.label_CANNY.setText(_translate("Ventana_DETECCIONBORDE3", "Canny"))
        self.groupBox_2.setToolTip(_translate("Ventana_DETECCIONBORDE3", "<html><head/><body><p><br/></p></body></html>"))
        self.comboBox_KERNELSIZE_CANNY.setItemText(0, _translate("Ventana_DETECCIONBORDE3", "0"))
        self.comboBox_KERNELSIZE_CANNY.setItemText(1, _translate("Ventana_DETECCIONBORDE3", "3"))
        self.comboBox_KERNELSIZE_CANNY.setItemText(2, _translate("Ventana_DETECCIONBORDE3", "5"))
        self.comboBox_KERNELSIZE_CANNY.setItemText(3, _translate("Ventana_DETECCIONBORDE3", "7"))
        self.label_preFILTRO_CANNY.setText(_translate("Ventana_DETECCIONBORDE3", "Prefiltro:"))
        self.label_KERNEL_CANNY.setText(_translate("Ventana_DETECCIONBORDE3", "Tamaño del kernel"))
        self.label_GASSIANBLUR_CANNY.setText(_translate("Ventana_DETECCIONBORDE3", "Gaussian Blur"))
        self.label_UMBRAL_MIN_CANNY.setText(_translate("Ventana_DETECCIONBORDE3", "Umbral Min:"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(0, _translate("Ventana_DETECCIONBORDE3", "0"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(1, _translate("Ventana_DETECCIONBORDE3", "1"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(2, _translate("Ventana_DETECCIONBORDE3", "2"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(3, _translate("Ventana_DETECCIONBORDE3", "3"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(4, _translate("Ventana_DETECCIONBORDE3", "4"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(5, _translate("Ventana_DETECCIONBORDE3", "5"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(6, _translate("Ventana_DETECCIONBORDE3", "6"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(7, _translate("Ventana_DETECCIONBORDE3", "7"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(8, _translate("Ventana_DETECCIONBORDE3", "8"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(9, _translate("Ventana_DETECCIONBORDE3", "9"))
        self.comboBox_UMBRAL_MIN_CANNY.setItemText(10, _translate("Ventana_DETECCIONBORDE3", "10"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(0, _translate("Ventana_DETECCIONBORDE3", "0"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(1, _translate("Ventana_DETECCIONBORDE3", "1"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(2, _translate("Ventana_DETECCIONBORDE3", "2"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(3, _translate("Ventana_DETECCIONBORDE3", "3"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(4, _translate("Ventana_DETECCIONBORDE3", "4"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(5, _translate("Ventana_DETECCIONBORDE3", "5"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(6, _translate("Ventana_DETECCIONBORDE3", "6"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(7, _translate("Ventana_DETECCIONBORDE3", "7"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(8, _translate("Ventana_DETECCIONBORDE3", "8"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(9, _translate("Ventana_DETECCIONBORDE3", "9"))
        self.comboBox_UMBRAL_MAX_CANNY.setItemText(10, _translate("Ventana_DETECCIONBORDE3", "10"))
        self.label_UMBRAL_MAX_CANNY.setText(_translate("Ventana_DETECCIONBORDE3", "Umbral Max:"))
        self.radioButton_VENTANA_B.setText(_translate("Ventana_DETECCIONBORDE3", "Ventana B"))
        self.radioButton_VENTANA_A.setText(_translate("Ventana_DETECCIONBORDE3", "Ventana A"))
        self.groupBox_IMG_ORIGINAL.setTitle(_translate("Ventana_DETECCIONBORDE3", "Imagen original"))
        self.groupBox_IMG_RESULTANTE.setTitle(_translate("Ventana_DETECCIONBORDE3", "Imagen Procesada"))
        self.tabWidget_FILTROS.setTabText(self.tabWidget_FILTROS.indexOf(self.tab), _translate("Ventana_DETECCIONBORDE3", "Filtro Gaussian"))
        self.tabWidget_FILTROS.setTabText(self.tabWidget_FILTROS.indexOf(self.tab_SOBEL_X), _translate("Ventana_DETECCIONBORDE3", "Sobel X"))
        self.tabWidget_FILTROS.setTabText(self.tabWidget_FILTROS.indexOf(self.tab_SOBEL_Y), _translate("Ventana_DETECCIONBORDE3", "Sobel Y"))
        self.tabWidget_FILTROS.setTabText(self.tabWidget_FILTROS.indexOf(self.tab_MAGNITUD), _translate("Ventana_DETECCIONBORDE3", "Magnitud"))
        self.tabWidget_FILTROS.setTabText(self.tabWidget_FILTROS.indexOf(self.tab_LAPLACIANO), _translate("Ventana_DETECCIONBORDE3", "Laplace"))
        self.tabWidget_FILTROS.setTabText(self.tabWidget_FILTROS.indexOf(self.tab_CANNY), _translate("Ventana_DETECCIONBORDE3", "CANNY"))
    
    '''---------------   Definiciones propias o métodos a usar   -----------'''
    def show_IMG_1 (self):
        global fileName_IMG
        
        fileName_IMG = self.abrir_imagen()
        self.photo_ORIGINAL.setPixmap(QtGui.QPixmap(fileName_IMG))
            
    def show_IMG_2(self):
        try:
            
            if self.radioButton_VENTANA_A.isChecked()==True: 
                if self.radioButton_LAPLACE.isChecked()==True:
                    global nameImg_filtroGauss, nameImg_LAPLACE
                    
                    nameImg_filtroGauss, nameImg_LAPLACE = filtros_SLC.iniciarFiltroLaplacebyOrder ( fileName_IMG, self.obtenerValorKERNELSIZE(), self.obtenerValorErdenGaussian() )
                    self.photo_RESULTANTE_LAPLACE.setPixmap(QtGui.QPixmap( nameImg_LAPLACE ))
                    self.photo_RESULTANTE_PREFILTRO_GAUSSIAN.setPixmap(QtGui.QPixmap( nameImg_filtroGauss ))
                    
                elif self.radioButton_SOBEL.isChecked()==True: 
                    global nameImg_sobelX, nameImg_sobelY, nameImg_MAGNITUD
                    
                    nameImg_filtroGauss, nameImg_sobelX, nameImg_sobelY, nameImg_MAGNITUD = filtros_SLC.iniciarFiltroSobelbyOrder ( fileName_IMG, self.obtenerValorKERNELSIZE(), self.obtenerValorErdenGaussian()  )
                    self.photo_RESULTANTE_SX.setPixmap(QtGui.QPixmap( nameImg_sobelX ))
                    self.photo_RESULTANTE_SY.setPixmap(QtGui.QPixmap( nameImg_sobelY ))
                    self.photo_RESULTANTE_MAGNITUD.setPixmap(QtGui.QPixmap( nameImg_MAGNITUD ))
                    self.photo_RESULTANTE_PREFILTRO_GAUSSIAN.setPixmap(QtGui.QPixmap( nameImg_filtroGauss ))
                else:
                    print ('No hay nada mas que hacer1')
                    
            elif self.radioButton_VENTANA_B.isChecked()==True:
                global nameImg_GaussianBlur, nameImg_CANNY
                
                nameImg_GaussianBlur, nameImg_CANNY = filtros_SLC.iniciarFiltroCanny ( fileName_IMG, self.obtenerVKERNELcanny(), self.obtenerVthreshold1canny(), self.obtenerVthreshold1canny() )
                self.photo_RESULTANTE_CANNY.setPixmap(QtGui.QPixmap( nameImg_CANNY ))
                self.photo_RESULTANTE_PREFILTRO_GAUSSIAN.setPixmap(QtGui.QPixmap( nameImg_GaussianBlur ))
            else:
                print ('No hay nada mas que hacer2')
            
        except:
            self.clickMethod()
                
    def clickMethod(self):
        q = QMessageBox(QMessageBox.Warning, extentionImg.typeAlert, extentionImg.typeMessage)
        q.setStandardButtons(QMessageBox.Ok)
        q.exec_()
            
    def abrir_imagen(self):
        title = extentionImg.titulo
        qfd = QFileDialog()
        path = paths.PATH
        filter = extentionImg.filtroIMG
        f, _ = QFileDialog.getOpenFileName(qfd, title, path, filter)
        return str(f)
    
    def obtenerValorKERNELSIZE(self):
        kernel_text = self.comboBox_KERNELSIZE.currentIndex()
        if kernel_text == 0: return 3
        if kernel_text == 1: return 5
        if kernel_text == 2: return 7
    
    def obtenerValorErdenGaussian(self):
        Gaussian_text = self.comboBox_FILTRO_GAUSS12.currentIndex()
        if Gaussian_text == 0: return 0
        if Gaussian_text == 1: return 1
        if Gaussian_text == 2: return 2 
        
    def obtenerVKERNELcanny(self):
        kernelcanny = self.comboBox_KERNELSIZE_CANNY.currentIndex()
        if kernelcanny == 0: return 0
        if kernelcanny == 1: return 3
        if kernelcanny == 2: return 5
        if kernelcanny == 3: return 7
        
    def obtenerVthreshold1canny(self):
        threshold1 = self.comboBox_UMBRAL_MIN_CANNY.currentIndex()
        if threshold1 == 0: return 0
        if threshold1 == 1: return 1
        if threshold1 == 2: return 2
        if threshold1 == 3: return 3
        if threshold1 == 4: return 4
        if threshold1 == 5: return 5
        if threshold1 == 6: return 6
        if threshold1 == 7: return 7
        if threshold1 == 8: return 8
        if threshold1 == 9: return 9
        if threshold1 == 10: return 10
        if threshold1 == 11: return 11
        
    def obtenerVthreshold2canny(self):
        threshold2 = self.comboBox_UMBRAL_MAX_CANNY.currentIndex()
        if threshold2 == 0: return 0
        if threshold2 == 1: return 1
        if threshold2 == 2: return 2
        if threshold2 == 3: return 3
        if threshold2 == 4: return 4
        if threshold2 == 5: return 5
        if threshold2 == 6: return 6
        if threshold2 == 7: return 7
        if threshold2 == 8: return 8
        if threshold2 == 9: return 9
        if threshold2 == 10: return 10
        if threshold2 == 11: return 11
    '''---------------------------------------------------------------------'''
    


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Ventana_DETECCIONBORDE3 = QtWidgets.QMainWindow()
    ui = Ui_Ventana_DETECCIONBORDE3()
    ui.setupUi(Ventana_DETECCIONBORDE3)
    Ventana_DETECCIONBORDE3.show()
    sys.exit(app.exec_())

