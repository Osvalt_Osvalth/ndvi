# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 21:44:44 2020
@author: music
"""
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QMenuBar, QMainWindow, QWidget, QPushButton, QAction

from MainWindows import Ui_principal_mainWINDOW #Principal
# SUBVENTANAS
from v_OPERACIONES import Ui_VENTANA_OPERACIONES
from v_CLUSTERING import Ui_MainWindow
from v_GRABCUT import Ui_ventanaGRACUT
from v_FILTROSUAVIZADO import Ui_Ventana_MEDIANBLUR
from v_REALCEBORDES import Ui_VENTANArealceDeBordes
from v_DETECCIONBORDES import Ui_Ventana_DETECCIONBORDE3
from v_ECUALIZACION import Ui_Ventana_ECUALIZACION
from v_NDVI import Ui_Ventana_NDVI
from v_COLABORACION import Ui_VENT_COLABORACION


class MainWindow(QtWidgets.QMainWindow, Ui_principal_mainWINDOW):
    
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.setupUi(self)
        
        #LLAMADO DE CADA VENTANA MEDIANTE EL USO DEL MENÚ
        self.action_SUMARESTA.triggered.connect( self.abrir_v_OPERACIONES )
        self.action_CLUSTERING.triggered.connect( self.abrir_v_CLUSTERING )
        self.action_GRABCUT.triggered.connect( self.abrir_v_GRABCUT )
        self.action_SUAVIZADO_PASOBAJO.triggered.connect( self.abrir_v_FILTROSUAVIZADO )
        self.action_REALCEDEBORDES_PASOALTO.triggered.connect( self.abrir_v_REALCEBORDES )
        self.action_BOBEL_LAPLACE_CANNY.triggered.connect( self.abrir_v_DETECCIONBORDES )
        self.action_ECUALIZAR.triggered.connect( self.abrir_v_ECUALIZACION )
        self.action_NDVI.triggered.connect( self.abrir_v_NDVI )
        self.action_ACERCA_DE.triggered.connect(  self.abrir_v_COLABORACION )
        
        
    def abrir_v_OPERACIONES(self):
        self.v_OPERACIONES = QtWidgets.QMainWindow()
        self.ui = Ui_VENTANA_OPERACIONES()
        self.ui.setupUi(self.v_OPERACIONES)
        self.mdiArea.addSubWindow(self.v_OPERACIONES)
        self.v_OPERACIONES.show()
    
    def abrir_v_CLUSTERING(self):
        self.v_CLUSTERING = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.v_CLUSTERING)
        self.mdiArea.addSubWindow(self.v_CLUSTERING)
        self.v_CLUSTERING.show()
        
    def abrir_v_GRABCUT(self):
        self.v_GRABCUT = QtWidgets.QMainWindow()
        self.ui = Ui_ventanaGRACUT()
        self.ui.setupUi(self.v_GRABCUT)
        self.mdiArea.addSubWindow(self.v_GRABCUT)
        self.v_GRABCUT.show()
        
    def abrir_v_FILTROSUAVIZADO(self):
        self.v_FILTROSUAVIZADO = QtWidgets.QMainWindow()
        self.ui = Ui_Ventana_MEDIANBLUR()
        self.ui.setupUi(self.v_FILTROSUAVIZADO)
        self.mdiArea.addSubWindow(self.v_FILTROSUAVIZADO)
        self.v_FILTROSUAVIZADO.show()
    
    def abrir_v_REALCEBORDES(self):
        self.v_REALCEBORDES = QtWidgets.QMainWindow()
        self.ui = Ui_VENTANArealceDeBordes()
        self.ui.setupUi(self.v_REALCEBORDES)
        self.mdiArea.addSubWindow(self.v_REALCEBORDES)
        self.v_REALCEBORDES.show()
    
    def abrir_v_DETECCIONBORDES(self):
        self.v_DETECCIONBORDES = QtWidgets.QMainWindow()
        self.ui = Ui_Ventana_DETECCIONBORDE3()
        self.ui.setupUi(self.v_DETECCIONBORDES)
        self.mdiArea.addSubWindow(self.v_DETECCIONBORDES)
        self.v_DETECCIONBORDES.show()
    
    def abrir_v_ECUALIZACION(self):
        self.v_ECUALIZACION = QtWidgets.QMainWindow()
        self.ui = Ui_Ventana_ECUALIZACION()
        self.ui.setupUi(self.v_ECUALIZACION)
        self.mdiArea.addSubWindow(self.v_ECUALIZACION)
        self.v_ECUALIZACION.show()
        
    def abrir_v_NDVI(self):
        self.v_NDVI = QtWidgets.QMainWindow()
        self.ui = Ui_Ventana_NDVI()
        self.ui.setupUi(self.v_NDVI)
        self.mdiArea.addSubWindow(self.v_NDVI)
        self.v_NDVI.show()
    
    def abrir_v_COLABORACION(self):
        self.v_COLABORACION = QtWidgets.QMainWindow()
        self.ui = Ui_VENT_COLABORACION()
        self.ui.setupUi(self.v_COLABORACION)
        self.mdiArea.addSubWindow(self.v_COLABORACION)
        self.v_COLABORACION.show()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec_())