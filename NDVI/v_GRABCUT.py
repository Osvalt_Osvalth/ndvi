# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventana_GRABCUT_X.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QPushButton
from grabcut import grabcut  as gC
from NDVI_files import rutas_D_Archivos as paths
from TipoIMG import configuracion as extentionImg

class Ui_ventanaGRACUT(object):
    def setupUi(self, ventanaGRACUT):
        ventanaGRACUT.setObjectName("ventanaGRACUT")
        ventanaGRACUT.resize(490, 400)
        ventanaGRACUT.setMaximumSize(QtCore.QSize(490, 400))
        ventanaGRACUT.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        ventanaGRACUT.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.centralwidget = QtWidgets.QWidget(ventanaGRACUT)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(8, 0, 471, 371))
        self.groupBox.setObjectName("groupBox")
        self.btn_ABRIR = QtWidgets.QPushButton(self.groupBox)
        self.btn_ABRIR.setGeometry(QtCore.QRect(170, 330, 121, 31))
        self.btn_ABRIR.setObjectName("btn_ABRIR")
        self.tableWidget = QtWidgets.QTableWidget(self.groupBox)
        self.tableWidget.setGeometry(QtCore.QRect(10, 20, 451, 171))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tableWidget.sizePolicy().hasHeightForWidth())
        self.tableWidget.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.tableWidget.setFont(font)
        self.tableWidget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.tableWidget.setAutoFillBackground(False)
        self.tableWidget.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.tableWidget.setFrameShadow(QtWidgets.QFrame.Raised)
        self.tableWidget.setLineWidth(5)
        self.tableWidget.setMidLineWidth(8)
        self.tableWidget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.tableWidget.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.tableWidget.setAutoScroll(False)
        self.tableWidget.setAutoScrollMargin(10)
        self.tableWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.tableWidget.setTabKeyNavigation(False)
        self.tableWidget.setProperty("showDropIndicator", False)
        self.tableWidget.setDragDropOverwriteMode(False)
        self.tableWidget.setAlternatingRowColors(True)
        self.tableWidget.setTextElideMode(QtCore.Qt.ElideNone)
        self.tableWidget.setShowGrid(True)
        self.tableWidget.setGridStyle(QtCore.Qt.DashLine)
        self.tableWidget.setWordWrap(False)
        self.tableWidget.setCornerButtonEnabled(True)
        self.tableWidget.setRowCount(6)
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setObjectName("tableWidget")
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setItem(0, 0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.tableWidget.setItem(0, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setItem(1, 0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.tableWidget.setItem(1, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setItem(2, 0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.tableWidget.setItem(2, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setItem(3, 0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.tableWidget.setItem(3, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setItem(4, 0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.tableWidget.setItem(4, 1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setItem(5, 0, item)
        item = QtWidgets.QTableWidgetItem()
        item.setTextAlignment(QtCore.Qt.AlignCenter)
        self.tableWidget.setItem(5, 1, item)
        self.tableWidget.horizontalHeader().setVisible(True)
        self.tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.tableWidget.horizontalHeader().setDefaultSectionSize(221)
        self.tableWidget.horizontalHeader().setHighlightSections(False)
        self.tableWidget.horizontalHeader().setMinimumSectionSize(38)
        self.tableWidget.horizontalHeader().setSortIndicatorShown(False)
        self.tableWidget.horizontalHeader().setStretchLastSection(False)
        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.verticalHeader().setDefaultSectionSize(23)
        self.tableWidget.verticalHeader().setHighlightSections(True)
        self.tableWidget.verticalHeader().setMinimumSectionSize(13)
        self.tableWidget.verticalHeader().setSortIndicatorShown(False)
        self.tableWidget.verticalHeader().setStretchLastSection(False)
        self.textEdit = QtWidgets.QTextEdit(self.groupBox)
        self.textEdit.setGeometry(QtCore.QRect(10, 210, 451, 111))
        self.textEdit.setMaximumSize(QtCore.QSize(16777215, 450))
        self.textEdit.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.textEdit.setCursorWidth(0)
        self.textEdit.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.textEdit.setObjectName("textEdit")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setGeometry(QtCore.QRect(400, 190, 61, 21))
        self.label.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse)
        self.label.setObjectName("label")
        ventanaGRACUT.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(ventanaGRACUT)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 490, 21))
        self.menubar.setObjectName("menubar")
        ventanaGRACUT.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(ventanaGRACUT)
        self.statusbar.setObjectName("statusbar")
        ventanaGRACUT.setStatusBar(self.statusbar)

        self.retranslateUi(ventanaGRACUT)
        self.btn_ABRIR.clicked.connect( self.seleccionarImagen )
        QtCore.QMetaObject.connectSlotsByName(ventanaGRACUT)

    def retranslateUi(self, ventanaGRACUT):
        _translate = QtCore.QCoreApplication.translate
        ventanaGRACUT.setWindowTitle(_translate("ventanaGRACUT", "Segmentación - Grabcut"))
        self.groupBox.setTitle(_translate("ventanaGRACUT", "Acciones"))
        self.btn_ABRIR.setText(_translate("ventanaGRACUT", "Abrir"))
        item = self.tableWidget.verticalHeaderItem(0)
        item.setText(_translate("ventanaGRACUT", "1"))
        item = self.tableWidget.verticalHeaderItem(1)
        item.setText(_translate("ventanaGRACUT", "2"))
        item = self.tableWidget.verticalHeaderItem(2)
        item.setText(_translate("ventanaGRACUT", "3"))
        item = self.tableWidget.verticalHeaderItem(3)
        item.setText(_translate("ventanaGRACUT", "4"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("ventanaGRACUT", "Opcciones"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("ventanaGRACUT", "Teclas"))
        __sortingEnabled = self.tableWidget.isSortingEnabled()
        self.tableWidget.setSortingEnabled(False)
        item = self.tableWidget.item(0, 0)
        item.setText(_translate("ventanaGRACUT", "Segmentar"))
        item = self.tableWidget.item(0, 1)
        item.setText(_translate("ventanaGRACUT", "S"))
        item = self.tableWidget.item(1, 0)
        item.setText(_translate("ventanaGRACUT", "Área de imagen no probable"))
        item = self.tableWidget.item(1, 1)
        item.setText(_translate("ventanaGRACUT", "0"))
        item = self.tableWidget.item(2, 0)
        item.setText(_translate("ventanaGRACUT", "Área de imagen si probable"))
        item = self.tableWidget.item(2, 1)
        item.setText(_translate("ventanaGRACUT", "1"))
        item = self.tableWidget.item(3, 0)
        item.setText(_translate("ventanaGRACUT", "Reiniciar"))
        item = self.tableWidget.item(3, 1)
        item.setText(_translate("ventanaGRACUT", "R"))
        item = self.tableWidget.item(4, 0)
        item.setText(_translate("ventanaGRACUT", "Guardar"))
        item = self.tableWidget.item(4, 1)
        item.setText(_translate("ventanaGRACUT", "G"))
        item = self.tableWidget.item(5, 0)
        item.setText(_translate("ventanaGRACUT", "Salir"))
        item = self.tableWidget.item(5, 1)
        item.setText(_translate("ventanaGRACUT", "Esc"))
        self.tableWidget.setSortingEnabled(__sortingEnabled)
        self.textEdit.setHtml(_translate("ventanaGRACUT", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">1.-Dibuje un rectángulo con el </span><span style=\" font-size:12pt; font-weight:600;\">botón derecho</span><span style=\" font-size:12pt;\"> del mouse.</span></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">2.-Oprima varias veces la tecla </span><span style=\" font-size:12pt; font-weight:600;\">S</span><span style=\" font-size:12pt;\"> para ver los cambios o </span><span style=\" font-size:12pt; font-weight:600;\">actualizar</span><span style=\" font-size:12pt;\">.</span></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">    En los siguientes pasos uso el </span><span style=\" font-size:12pt; font-weight:600;\">botón izquierdo</span><span style=\" font-size:12pt;\"> del mouse</span></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">3.-Oprima la tecla </span><span style=\" font-size:12pt; font-weight:600;\">0</span><span style=\" font-size:12pt;\"> para eliminar </span><span style=\" font-size:12pt; font-weight:600; text-decoration: underline;\">elementos no probables</span><span style=\" font-size:12pt;\"> sobre la región deseada.</span></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">4.-Oprima la tecla </span><span style=\" font-size:12pt; font-weight:600;\">1</span><span style=\" font-size:12pt;\"> para seleccionar </span><span style=\" font-size:12pt; font-weight:600; text-decoration: underline;\">elementos probables</span><span style=\" font-size:12pt;\"> a destacar sobre la región deseada.</span></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">5.-Oprima la tecla </span><span style=\" font-size:12pt; font-weight:600;\">R</span><span style=\" font-size:12pt;\"> para reiniciar el proceso.</span></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">6.-Oprima la tecla </span><span style=\" font-size:12pt; font-weight:600;\">G</span><span style=\" font-size:12pt;\"> para guardar los cambios de la imagen.</span></p>\n"
"<p align=\"justify\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">7.-Oprima la tecla </span><span style=\" font-size:12pt; font-weight:600;\">Esc</span><span style=\" font-size:12pt;\"> para finalizar y salir.</span></p></body></html>"))
        self.label.setText(_translate("ventanaGRACUT", "Indicaciones"))
    
    def seleccionarImagen (self):
        imageN = self.abrir_imagen()
        if imageN:
            gC.getGrabcut( imageN )
        else:
            self.clickMethod()
            
    def clickMethod(self):
        q = QMessageBox(QMessageBox.Warning, extentionImg.typeAlert, extentionImg.typeMessage)
        q.setStandardButtons(QMessageBox.Ok)
        q.exec_()
                         
    def abrir_imagen(self):
        title = extentionImg.titulo
        qfd = QFileDialog()
        path = paths.PATH
        filter = extentionImg.filtroIMG
        f, _ = QFileDialog.getOpenFileName(qfd, title, path, filter)
        return str(f)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ventanaGRACUT = QtWidgets.QMainWindow()
    ui = Ui_ventanaGRACUT()
    ui.setupUi(ventanaGRACUT)
    ventanaGRACUT.show()
    sys.exit(app.exec_())

