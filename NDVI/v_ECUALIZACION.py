# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventana_EcualizaciónDeHistogramas.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QPushButton
from NDVI_files import rutas_D_Archivos as paths
# from HISTOGRAMA.histograma2 import getHistogram
from ecualizacion import ecualizar as EQu
from TipoIMG import configuracion as extentionImg

class Ui_Ventana_ECUALIZACION(object):
    def setupUi(self, Ventana_ECUALIZACION):
        Ventana_ECUALIZACION.setObjectName("Ventana_ECUALIZACION")
        Ventana_ECUALIZACION.resize(800, 565)
        Ventana_ECUALIZACION.setMinimumSize(QtCore.QSize(800, 565))
        Ventana_ECUALIZACION.setMaximumSize(QtCore.QSize(800, 565))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        Ventana_ECUALIZACION.setFont(font)
        Ventana_ECUALIZACION.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        Ventana_ECUALIZACION.setMouseTracking(False)
        Ventana_ECUALIZACION.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.centralwidget = QtWidgets.QWidget(Ventana_ECUALIZACION)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox_ACCIONES = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_ACCIONES.setGeometry(QtCore.QRect(10, 0, 251, 111))
        self.groupBox_ACCIONES.setObjectName("groupBox_ACCIONES")
        self.btn_ABRIR = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_ABRIR.setGeometry(QtCore.QRect(14, 20, 111, 30))
        self.btn_ABRIR.setObjectName("btn_ABRIR")
        self.btn_HISTOGRAMA = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_HISTOGRAMA.setGeometry(QtCore.QRect(130, 20, 111, 30))
        self.btn_HISTOGRAMA.setObjectName("btn_HISTOGRAMA")
        self.btn_ecualizacion = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_ecualizacion.setGeometry(QtCore.QRect(14, 60, 111, 30))
        self.btn_ecualizacion.setObjectName("btn_ecualizacion")
        self.groupBox_IMG_ORIGINAL = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_IMG_ORIGINAL.setGeometry(QtCore.QRect(270, 0, 261, 171))
        self.groupBox_IMG_ORIGINAL.setObjectName("groupBox_IMG_ORIGINAL")
        self.photo_ORIGINAL = QtWidgets.QLabel(self.groupBox_IMG_ORIGINAL)
        self.photo_ORIGINAL.setGeometry(QtCore.QRect(10, 25, 241, 131))
        self.photo_ORIGINAL.setText("")
        self.photo_ORIGINAL.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_ORIGINAL.setScaledContents(True)
        self.photo_ORIGINAL.setObjectName("photo_ORIGINAL")
        self.groupBox_HISTOGRAMA = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_HISTOGRAMA.setGeometry(QtCore.QRect(540, 0, 251, 171))
        self.groupBox_HISTOGRAMA.setObjectName("groupBox_HISTOGRAMA")
        self.photo_HISTOGRAMA = QtWidgets.QLabel(self.groupBox_HISTOGRAMA)
        self.photo_HISTOGRAMA.setGeometry(QtCore.QRect(10, 20, 231, 141))
        self.photo_HISTOGRAMA.setText("")
        self.photo_HISTOGRAMA.setPixmap(QtGui.QPixmap("../imgs/IMG_1593142839540_HISTOGRAMA.TIF"))
        self.photo_HISTOGRAMA.setScaledContents(True)
        self.photo_HISTOGRAMA.setObjectName("photo_HISTOGRAMA")
        self.groupBox_IMG_RESULTANTE = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_IMG_RESULTANTE.setGeometry(QtCore.QRect(10, 170, 781, 381))
        self.groupBox_IMG_RESULTANTE.setObjectName("groupBox_IMG_RESULTANTE")
        self.photo_RESULTANTE = QtWidgets.QLabel(self.groupBox_IMG_RESULTANTE)
        self.photo_RESULTANTE.setGeometry(QtCore.QRect(40, 20, 711, 351))
        self.photo_RESULTANTE.setText("")
        self.photo_RESULTANTE.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTANTE.setScaledContents(True)
        self.photo_RESULTANTE.setObjectName("photo_RESULTANTE")
        Ventana_ECUALIZACION.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Ventana_ECUALIZACION)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        Ventana_ECUALIZACION.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(Ventana_ECUALIZACION)
        self.statusbar.setObjectName("statusbar")
        Ventana_ECUALIZACION.setStatusBar(self.statusbar)

        self.retranslateUi(Ventana_ECUALIZACION)
        self.btn_HISTOGRAMA.clicked.connect(self.mostrarHistogramaDeImagen)
        self.btn_ABRIR.clicked.connect(self.show_IMG_1)
        self.btn_ecualizacion.clicked.connect(self.show_IMG_2)
        QtCore.QMetaObject.connectSlotsByName(Ventana_ECUALIZACION)
        
        

    def retranslateUi(self, Ventana_ECUALIZACION):
        _translate = QtCore.QCoreApplication.translate
        Ventana_ECUALIZACION.setWindowTitle(_translate("Ventana_ECUALIZACION", "Ecualización de histogrma de una imagen"))
        self.groupBox_ACCIONES.setTitle(_translate("Ventana_ECUALIZACION", "Acciones"))
        self.btn_ABRIR.setText(_translate("Ventana_ECUALIZACION", "Abrir"))
        self.btn_HISTOGRAMA.setText(_translate("Ventana_ECUALIZACION", "Histograma"))
        self.btn_ecualizacion.setText(_translate("Ventana_ECUALIZACION", "Ecualización"))
        self.groupBox_IMG_ORIGINAL.setTitle(_translate("Ventana_ECUALIZACION", "Imagen original"))
        self.groupBox_HISTOGRAMA.setTitle(_translate("Ventana_ECUALIZACION", "Histograma"))
        self.groupBox_IMG_RESULTANTE.setTitle(_translate("Ventana_ECUALIZACION", "Imagen Procesada"))
    
    '''---------------   Definiciones propias o métodos a usar   -----------'''
    def show_IMG_1 (self):
        global fileName_IMG
        fileName_IMG = self.abrir_imagen()
        self.photo_ORIGINAL.setPixmap(QtGui.QPixmap(fileName_IMG))
        
    def show_IMG_2(self):
        global thisIMAGE, HistogramaPath
        try:
            thisIMAGE, HistogramaPath  = EQu.ecualizarHistograma ( fileName_IMG )
            self.photo_RESULTANTE.setPixmap(QtGui.QPixmap(thisIMAGE))
        except:
            self.clickMethod()
                
    
    def mostrarHistogramaDeImagen(self):
        try:
            # hISTO = getHistogram(thisIMAGE, HistogramaPath)
            # self.photo_HISTOGRAMA.setPixmap(QtGui.QPixmap(hISTO))
            self.photo_HISTOGRAMA.setPixmap(QtGui.QPixmap(HistogramaPath))
        except:
            self.clickMethod()
    
    def clickMethod(self):
        q = QMessageBox(QMessageBox.Warning, extentionImg.typeAlert, extentionImg.typeMessage)
        q.setStandardButtons(QMessageBox.Ok)
        q.exec_()
    
    def abrir_imagen(self):
        title = extentionImg.titulo
        qfd = QFileDialog()
        path = paths.PATH
        filter = extentionImg.filtroIMG
        f, _ = QFileDialog.getOpenFileName(qfd, title, path, filter)
        return str(f)
    '''---------------------------------------------------------------------'''

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Ventana_ECUALIZACION = QtWidgets.QMainWindow()
    ui = Ui_Ventana_ECUALIZACION()
    ui.setupUi(Ventana_ECUALIZACION)
    Ventana_ECUALIZACION.show()
    sys.exit(app.exec_())

