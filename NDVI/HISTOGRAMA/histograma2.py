# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 22:05:49 2020

@author: music
"""
import numpy as np
from matplotlib import pyplot as plt
from NDVI_files.leerIMGs_TIF import read_image_NIR as readGRAY

def getHistogram ( rutaIMG, nametoSave ):
    gray_img = readGRAY(rutaIMG)
    # hist = cv2.calcHist([gray_img],[0],None,[256],[0,256])
    plt.hist(gray_img.ravel(),256,[0,256])
    plt.title('Histograma')
    plt.xlim([0,256])
    plt.savefig(nametoSave,dpi=256)#2600
    plt.show()
    plt.close(1)
    print('saliendo'+nametoSave)
    return nametoSave