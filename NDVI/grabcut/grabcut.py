# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 21:00:12 2020

@author: music
uso :
    python grabcut.py <filename>
"""

import numpy as np
import cv2
import sys
from NDVI_files import rutas_D_Archivos as myPath

BLACK = [0,0,0]         # Color Negro Region no deseada
WHITE = [255,255,255]   # Color Blanco Region deseada

DRAW_BG = {'color' : BLACK, 'val' : 0}
DRAW_FG = {'color' : WHITE, 'val' : 1}

# Configuracion de banderas
rect = (0,0,1,1)
drawing = False         # bandera para dibujar el rectangulo
rectangle = False       # bandera para dibujar el rectangulo
rect_over = False       # bandera para checar si se dibujo el rectangulo
rect_or_mask = 100      # bandera para la seleccion de rectangulo o modo mascara
value = DRAW_FG         # inicializar bandera para el valor de 1 (region problable sobre imagen)
thickness = 6           # espesor del circulo para dibujar


def onmouse ( event, x, y, flags, param):
    global img, img2, drawing, value, mask, rectangle, rect, rect_or_mask, ix, iy, rect_over, output
        
    # Dibujar rectángulo
    if event == cv2.EVENT_RBUTTONDOWN:
        rectangle = True
        ix,iy = x,y

    elif event == cv2.EVENT_MOUSEMOVE:
        if rectangle == True:
            img = img2.copy()
            cv2.rectangle(img,(ix,iy),(x,y), [255,0,0] ,2)
            rect = (ix,iy,abs(ix-x),abs(iy-y))
            rect_or_mask = 0

    elif event == cv2.EVENT_RBUTTONUP:
        rectangle = False
        rect_over = True
        cv2.rectangle(img,(ix,iy),(x,y), [255,0,0] ,2)
        rect = (ix,iy,abs(ix-x),abs(iy-y))
        rect_or_mask = 0
        
    # Dibujar curvas de retoque
    if event == cv2.EVENT_LBUTTONDOWN:
        if rect_over == False:
            print ("Primero dibuje un rectángulo \n")
        else:
            drawing = True
            cv2.circle(img,(x,y),thickness,value['color'],-1)
            cv2.circle(mask,(x,y),thickness,value['val'],-1)

    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            cv2.circle(img,(x,y),thickness,value['color'],-1)
            cv2.circle(mask,(x,y),thickness,value['val'],-1)

    elif event == cv2.EVENT_LBUTTONUP:
        if drawing == True:
            drawing = False
            cv2.circle(img,(x,y),thickness,value['color'],-1)
            cv2.circle(mask,(x,y),thickness,value['val'],-1)

def getGrabcut( nameImagenPath ):
    global img,img2,drawing,value,mask,rectangle,rect,rect_or_mask,ix,iy,rect_over, output
    img = cv2.imread(nameImagenPath)
    img2 = img.copy()                               # a copy of original image
    mask = np.zeros(img.shape[:2],dtype = np.uint8) # mask initialized to PR_BG
    output = np.zeros(img.shape,np.uint8)           # output image to be shown
    
    cv2.namedWindow('Imagen de salida caracterizada')
    cv2.namedWindow('Imagen de entrada')
    cv2.setMouseCallback('Imagen de entrada',onmouse)
    cv2.moveWindow('Imagen de entrada',img.shape[1]+10,10)
        
    while( 1 ):
                        
        cv2.imshow('Imagen de salida caracterizada',output)
        cv2.imshow('Imagen de entrada',img)
        cv2.waitKey(1)
        k = 0xFF & cv2.waitKey(1)
        if k == 27:         # esc to exit
            break
        elif k == ord('0'): # BG drawing
            value = DRAW_BG
        elif k == ord('1'): # FG drawing
            value = DRAW_FG
        elif k == ord('g'): # save image
            nameImg = myPath.getPathByCategory( 'GRABCUT', histo = None )            
            cv2.imwrite( nameImg ,output )
        elif k == ord('r'): # reset everything
            rect = (0,0,1,1)
            drawing = False         
            rectangle = False       
            rect_or_mask = 100 
            rect_over = False     
            value = DRAW_FG         
            img = img2.copy()
            mask = np.zeros(img.shape[:2],dtype = np.uint8) # mask initialized to PR_BG
            output = np.zeros(img.shape,np.uint8)           # output image to be shown
        elif k == ord('s'): # segment the image
            if (rect_or_mask == 0):         # grabcut with rect
                bgdmodel = np.zeros((1,65),np.float64)
                fgdmodel = np.zeros((1,65),np.float64)    
                cv2.grabCut(img2,mask,rect,bgdmodel,fgdmodel,1,cv2.GC_INIT_WITH_RECT)
                rect_or_mask = 1
            elif rect_or_mask == 1:         # grabcut with mask
                bgdmodel = np.zeros((1,65),np.float64)
                fgdmodel = np.zeros((1,65),np.float64) 
                cv2.grabCut(img2,mask,rect,bgdmodel,fgdmodel,1,cv2.GC_INIT_WITH_MASK)
    
        mask2 = np.where((mask==1) + (mask==3),255,0).astype('uint8')
        output = cv2.bitwise_and(img2,img2,mask=mask2)   
        
        
    cv2.destroyAllWindows()
    