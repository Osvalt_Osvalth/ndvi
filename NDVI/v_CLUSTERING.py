# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventana_CLUSTERING.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QPushButton

from NDVI_files import rutas_D_Archivos as paths
from puntosD_interes import clustering as Segmentar
from TipoIMG import configuracion as extentionImg

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(729, 593)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        MainWindow.setFont(font)
        MainWindow.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        MainWindow.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox_IMG_ORIGINAL = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_IMG_ORIGINAL.setGeometry(QtCore.QRect(410, 0, 311, 171))
        self.groupBox_IMG_ORIGINAL.setObjectName("groupBox_IMG_ORIGINAL")
        self.photo_ORIGINAL = QtWidgets.QLabel(self.groupBox_IMG_ORIGINAL)
        self.photo_ORIGINAL.setGeometry(QtCore.QRect(40, 20, 242, 141))
        self.photo_ORIGINAL.setText("")
        self.photo_ORIGINAL.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_ORIGINAL.setScaledContents(True)
        self.photo_ORIGINAL.setObjectName("photo_ORIGINAL")
        self.groupBox_ACCIONES = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_ACCIONES.setGeometry(QtCore.QRect(10, 0, 391, 141))
        self.groupBox_ACCIONES.setObjectName("groupBox_ACCIONES")
        self.btn_ABRIR = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_ABRIR.setGeometry(QtCore.QRect(10, 20, 111, 30))
        self.btn_ABRIR.setObjectName("btn_ABRIR")
        self.groupBox = QtWidgets.QGroupBox(self.groupBox_ACCIONES)
        self.groupBox.setGeometry(QtCore.QRect(150, 20, 201, 101))
        self.groupBox.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.groupBox.setTitle("")
        self.groupBox.setObjectName("groupBox")
        self.label_preFILTRO = QtWidgets.QLabel(self.groupBox)
        self.label_preFILTRO.setGeometry(QtCore.QRect(30, 10, 51, 30))
        self.label_preFILTRO.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_preFILTRO.setObjectName("label_preFILTRO")
        self.horizontalSlider__KMEANS = QtWidgets.QSlider(self.groupBox)
        self.horizontalSlider__KMEANS.setGeometry(QtCore.QRect(30, 60, 151, 31))
        self.horizontalSlider__KMEANS.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.horizontalSlider__KMEANS.setMouseTracking(False)
        self.horizontalSlider__KMEANS.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.horizontalSlider__KMEANS.setMinimum(1)
        self.horizontalSlider__KMEANS.setMaximum(10)
        self.horizontalSlider__KMEANS.setSliderPosition(1)
        self.horizontalSlider__KMEANS.setTracking(False)
        self.horizontalSlider__KMEANS.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider__KMEANS.setObjectName("horizontalSlider__KMEANS")
        self.spinBox_KMEANS = QtWidgets.QSpinBox(self.groupBox)
        self.spinBox_KMEANS.setGeometry(QtCore.QRect(111, 10, 71, 31))
        self.spinBox_KMEANS.setKeyboardTracking(False)
        self.spinBox_KMEANS.setMinimum(1)
        self.spinBox_KMEANS.setMaximum(10)
        self.spinBox_KMEANS.setObjectName("spinBox_KMEANS")
        self.groupBox_IMG_RESULTANTE = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_IMG_RESULTANTE.setGeometry(QtCore.QRect(10, 170, 711, 381))
        self.groupBox_IMG_RESULTANTE.setObjectName("groupBox_IMG_RESULTANTE")
        self.tabWidget_IMAGENES = QtWidgets.QTabWidget(self.groupBox_IMG_RESULTANTE)
        self.tabWidget_IMAGENES.setEnabled(True)
        self.tabWidget_IMAGENES.setGeometry(QtCore.QRect(10, 20, 691, 351))
        self.tabWidget_IMAGENES.setToolTip("")
        self.tabWidget_IMAGENES.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.tabWidget_IMAGENES.setAutoFillBackground(False)
        self.tabWidget_IMAGENES.setTabPosition(QtWidgets.QTabWidget.South)
        self.tabWidget_IMAGENES.setTabShape(QtWidgets.QTabWidget.Triangular)
        self.tabWidget_IMAGENES.setTabsClosable(False)
        self.tabWidget_IMAGENES.setMovable(True)
        self.tabWidget_IMAGENES.setTabBarAutoHide(False)
        self.tabWidget_IMAGENES.setObjectName("tabWidget_IMAGENES")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.photo_RESULTANTE_SEGMENTACION = QtWidgets.QLabel(self.tab)
        self.photo_RESULTANTE_SEGMENTACION.setGeometry(QtCore.QRect(10, 10, 661, 311))
        self.photo_RESULTANTE_SEGMENTACION.setText("")
        self.photo_RESULTANTE_SEGMENTACION.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTANTE_SEGMENTACION.setScaledContents(True)
        self.photo_RESULTANTE_SEGMENTACION.setObjectName("photo_RESULTANTE_SEGMENTACION")
        self.tabWidget_IMAGENES.addTab(self.tab, "")
        self.tab_HISTOGRAMA = QtWidgets.QWidget()
        self.tab_HISTOGRAMA.setObjectName("tab_HISTOGRAMA")
        self.photo_HISTOGRAMA = QtWidgets.QLabel(self.tab_HISTOGRAMA)
        self.photo_HISTOGRAMA.setGeometry(QtCore.QRect(10, 10, 661, 311))
        self.photo_HISTOGRAMA.setText("")
        self.photo_HISTOGRAMA.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_HISTOGRAMA.setScaledContents(True)
        self.photo_HISTOGRAMA.setObjectName("photo_HISTOGRAMA")
        self.tabWidget_IMAGENES.addTab(self.tab_HISTOGRAMA, "")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 729, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget_IMAGENES.setCurrentIndex(1)
        self.btn_ABRIR.clicked.connect( self.show_IMG_1 )
        self.spinBox_KMEANS.valueChanged['int'].connect(self.horizontalSlider__KMEANS.setValue)
        self.horizontalSlider__KMEANS.valueChanged['int'].connect(self.spinBox_KMEANS.setValue)
        self.horizontalSlider__KMEANS.valueChanged['int'].connect( self.show_IMG_2 )
        # self.horizontalSlider__KMEANS.valueChanged['int'].connect(self.photo_HISTOGRAMA.show)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Clustering - Segmentación"))
        self.groupBox_IMG_ORIGINAL.setTitle(_translate("MainWindow", "Imagen original"))
        self.groupBox_ACCIONES.setTitle(_translate("MainWindow", "Acciones"))
        self.btn_ABRIR.setText(_translate("MainWindow", "Abrir"))
        self.btn_ABRIR.setShortcut(_translate("MainWindow", "Ctrl+A"))
        self.groupBox.setToolTip(_translate("MainWindow", "<html><head/><body><p><br/></p></body></html>"))
        self.label_preFILTRO.setText(_translate("MainWindow", "Kmeans"))
        self.groupBox_IMG_RESULTANTE.setTitle(_translate("MainWindow", "Imagen Procesada"))
        self.tabWidget_IMAGENES.setTabText(self.tabWidget_IMAGENES.indexOf(self.tab), _translate("MainWindow", "Segmentación"))
        self.tabWidget_IMAGENES.setTabText(self.tabWidget_IMAGENES.indexOf(self.tab_HISTOGRAMA), _translate("MainWindow", "Histograma"))
    
    '''---------------   Definiciones propias o métodos a usar   -----------'''
    def show_IMG_1 (self):
        global fileName_IMG
        
        fileName_IMG = self.abrir_imagen()
        self.photo_ORIGINAL.setPixmap(QtGui.QPixmap(fileName_IMG))
        
    def show_IMG_2(self):
        global thisIMAGE, HistogramaPath
        try:
            thisIMAGE, HistogramaPath  = Segmentar.getclustering ( fileName_IMG, self.getValueHoriz_KMEANS() )
            self.photo_RESULTANTE_SEGMENTACION.setPixmap(QtGui.QPixmap(thisIMAGE))
            self.photo_HISTOGRAMA.setPixmap(QtGui.QPixmap(HistogramaPath))
        except:
            self.clickMethod()
                
    def clickMethod(self):
        q = QMessageBox(QMessageBox.Warning, extentionImg.typeAlert, extentionImg.typeMessage)
        q.setStandardButtons(QMessageBox.Ok)
        q.exec_()
        
    def mostrarHistogramaDeImagen(self):
        return self.photo_HISTOGRAMA.setPixmap(QtGui.QPixmap(HistogramaPath))
    
    def abrir_imagen(self):
        title = extentionImg.titulo
        qfd = QFileDialog()
        path = paths.PATH
        filter = extentionImg.filtroIMG
        f, _ = QFileDialog.getOpenFileName(qfd, title, path, filter)
        return str(f)
    
    def getValueHoriz_KMEANS(self):
        return self.horizontalSlider__KMEANS.value()
    
    def getValueSPIN_KMEANS(self):
        return self.spinBox_KMEANS.value()
    
    '''---------------------------------------------------------------------'''



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

