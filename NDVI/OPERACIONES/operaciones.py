# -*- coding: utf-8 -*-
"""
Created on Sat Jul  4 21:10:16 2020

@author: music
https://omes-va.com/adicion-sustraccion-de-imagenes/
https://divyanshushekhar.com/transition-effect-opencv/amp/
"""
import cv2

from NDVI_files import leerIMGs_TIF as ReadWrittenIMGs
from NDVI_files import ndvi as ReadIMGs
from NDVI_files import rutas_D_Archivos as myPath

def getPathBynaryByName(nameImage , histo):
    guardarRutaCategoria = myPath.getPathByCategory(nameImage, histo)
    return guardarRutaCategoria

'''  Adición de imágenes con cv2.add
Para la adición de imágenes necesitamos la función cv2.add, en ella tenemos 
que especificar:

Imagen 1 (Primera matriz o escalar)
Imagen 2 (Segunda matriz o escalar)
'''
def getAdd ( ImG_Original_1, ImG_Original_2 ):
    imgORI1 = cv2.imread( ImG_Original_1 )
    imgORI2 = cv2.imread( ImG_Original_2 )
    while(1):
        nameImg = myPath.getPathByCategory('ADD', histo = None)
        resultado = cv2.add ( imgORI1, imgORI2 )
        ReadWrittenIMGs.writteImgBlack( nameImg, resultado )
        return nameImg

'''Mezcla de imágenes con cv2.addWeighted
Para la mezcla de imágenes necesitamos la función cv2.addWeighted, en ella 
tenemos que especificar:

Imagen 1 (Primera matriz)
Alpha (Peso de la primera matriz)
Imagen 2 (Segunda matriz)
Beta (Peso de la segunda matriz)
Gamma (Escalar añadido a la suma)

En cuanto a alpha y beta, estos son valores que se pueden tomar de 0 a 1 
para modificar la transparencia de la imagen. Al especificar 1 en alpha o 
beta, la imagen correspondiente se muestra tal cual es, mientras que si va
bajando el valor se va tornando transparente, hasta ser totalmente 
transparente en 0.
'''
def getaddWeighted ( ImG_Original_1, alpha, ImG_Original_2, betha, gamma=0 ):
    imgORI1 = cv2.imread( ImG_Original_1 )
    imgORI2 = cv2.imread( ImG_Original_2 )
    while(1):
        nameImg = myPath.getPathByCategory('ADDWEIGHTED', histo = None)
        resultado = cv2.addWeighted ( imgORI1, alpha/10, imgORI2, betha/10, gamma  )
        ReadWrittenIMGs.writteImgBlack( nameImg, resultado )
        return nameImg
# =============================================================================
#               '''    Sustracción de imágenes con OpenCV y Python   '''
# =============================================================================
'''Sustracción de imágenes con cv2.absdiff
cv2.absdiff calcula la diferencia absoluta entre los elementos de una imagen 
o un escalar, entonces debemos especificar:

Imagen 1 (Primera matriz o escalar)
Imagen 2 (Segunda matriz o escalar)

¿Cómo funciona cv2.absdiff?
Esta función calcula la diferencia en valor absoluto de dos valores, 
por lo tanto |65 – 226| = |-161|, ya que está en valor absoluto, su 
resultado final será 161.
'''
def getabsdiff ( ImG_Original_1, ImG_Original_2 ):
    imgORI1 = cv2.imread( ImG_Original_1 )
    imgORI2 = cv2.imread( ImG_Original_2 )
    while(1):
        nameImg = myPath.getPathByCategory('ABSDIFF', histo = None)
        resultado = cv2.absdiff ( imgORI1, imgORI2 )
        ReadWrittenIMGs.writteImgBlack( nameImg, resultado )
        return nameImg

'''Sustracción de imágenes con cv2.subtract
Al usar la función cv2.subtract, debemos especificar:

Imagen 1 (Primera matriz o escalar)
Imagen 2 (Segunda matriz o escalar)

¿Cómo funciona cv2.subtract?
Si restamos 65 – 226, tenemos un total de -161, sin embargo, 
vemos que en resultado[0,0] se obtiene 0, esto es debido a que 
al usar cv2.subtract si el resultado es menor a 0, el valor vuelve a 0.
'''
def getSubtract ( ImG_Original_1, ImG_Original_2 ):
    imgORI1 = cv2.imread( ImG_Original_1 )
    imgORI2 = cv2.imread( ImG_Original_2 )
    while(1):
        nameImg = myPath.getPathByCategory('SUBTRACT', histo = None)
        resultado = cv2.subtract ( imgORI1, imgORI2 )
        ReadWrittenIMGs.writteImgBlack( nameImg, resultado )
        return nameImg


