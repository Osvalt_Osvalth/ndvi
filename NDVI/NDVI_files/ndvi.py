# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 15:37:59 2020

@author: music
"""
'''----------------------------------------------------Mis Librerias a usar '''
from NDVI_files import funciones_y_metodos as Midef
from NDVI_files import leerIMGs_TIF as ReadIMGs
# import rutas_D_Archivos as Path

'''     Se leen las imagenes    '''
def obtenerImgRED(pathRED):    
    return ReadIMGs.read_image_RED ( pathRED )

def obtenerImgNIR(pathNIR):    
    return ReadIMGs.read_image_NIR ( pathNIR )

'''     Computamos y obtenemos NDVI   '''
def calcularNDVI( imgNIR, imgRED):
    RED = obtenerImgRED( imgRED )
    NIR = obtenerImgNIR( imgNIR )    
    return Midef.get_ndvi ( RED, NIR )

'''------------------------------------------Mostramos imagen NDVI Con BARRA'''
def mostrarImgDelNDVI ( ruta_nir, ruta_red ):
    NDVI = calcularNDVI(ruta_nir, ruta_red)
    return Midef.imageProcessing ( NDVI )
    
# http://pythonlcpa.blogspot.com/p/blog-page_47.html
# https://www.youtube.com/watch?v=0hN6vSSHT0I
# https://www.tutorialspoint.com/pyqt/pyqt_qfiledialog_widget.htm


