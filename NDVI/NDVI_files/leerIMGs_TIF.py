# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 13:34:47 2020

@author: music
"""
import cv2
import numpy as np
from matplotlib import pyplot as plt

'''-------------------------------------Lectura de imagenes .TIF (NIR & RED)'''
def read_image_RED(red):
    RED = cv2.imread(red , 0)# el cero indica escala de grises, mientras el 1 indica escala de colores
    #RED = cv2.normalize(RED, None, 0, 255, cv2.NORM_MINMAX)
    return RED

def read_image_NIR(nir):
    NIR = cv2.imread(nir , 0)# cv.IMREAD_GRAYSCALE es lo mismo que poner cero (0)
    #NIR = cv2.normalize(NIR, None, 0, 255, cv2.NORM_MINMAX)
    return NIR

def read_image_COLOR(color):
    return cv2.imread(color , 1)
    
'''-----------------------------------Escritura de imagenes .TIF (NIR & RED)'''
def writteImgBlack (nameImg, image):
    cv2.imwrite( nameImg, image )

def readIMGSobel(imagenSobel):
    img0 = cv2.imread(imagenSobel,)
    # converting to gray scale 
    return cv2.cvtColor(img0, cv2.COLOR_BGR2GRAY)

def readIMGBORDER(imagen):
    img0 = cv2.imread(imagen,) 
    return cv2.cvtColor(img0, cv2.COLOR_BGR2GRAY) # converting to gray scale

def saveImgFILTER_SOBEL (nameImg, imag0Sobel):
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    plt.imshow(imag0Sobel, cmap = 'gray')
    plt.savefig(nameImg,dpi=256)#2600
    plt.close(1)
    

