# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 15:45:35 2020

@author: music
"""
from matplotlib.colors import LinearSegmentedColormap
'''-----------------------------------------Escala de colores Para la Barra '''
""" MAPEO DE LOS COLORES
    RED    ->  (1, 0, 0)
    YELLOW ->  (1, 1, 0)
    GREEN  ->  (0, 1, 0)
    se usa para customizar la imagen NDVI  y crear la BARRA DE COLORES
    Donde N puede variar de 3 a un valor alto en este caso 256 valores 
"""
cmapColores    = [ (0, 1, 0), (1, 1, 0), (1, 0, 0) ]  # Gren -> Yellow -> Red 
cmapColoresBar = [ (1, 0, 0), (1, 1, 0), (0, 1, 0) ]  # Red -> Yellow -> Green

cmapColores_1   = LinearSegmentedColormap.from_list('cmapColores', cmapColores, N=3)
cmapColores_BAR = LinearSegmentedColormap.from_list('cmapColoresBar', cmapColoresBar, N=3)
