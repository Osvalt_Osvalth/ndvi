# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 15:30:44 2020

@author: Osvalth

███╗░░██╗██████╗░██╗░░░██╗██╗
████╗░██║██╔══██╗██║░░░██║██║
██╔██╗██║██║░░██║╚██╗░██╔╝██║
██║╚████║██║░░██║░╚████╔╝░██║
██║░╚███║██████╔╝░░╚██╔╝░░██║
╚═╝░░╚══╝╚═════╝░░░░╚═╝░░░╚═╝
"""

##########################################################################     NDVI

'''--------------------------------------------------------Librerias a usar '''
import numpy as np
from matplotlib import pyplot as plt
import cv2

'''----------------------------------------------------Mis Librerias a usar '''
from NDVI_files import color_Barra as col_Bar
from NDVI_files import rutas_D_Archivos as myPath

'''---------------------------------------Mis Definiciones o métodos a usar '''
def get_ndvi(red, nir):
    '''Compute normalized difference vegetation index.'''
    np.seterr(divide='ignore', invalid='ignore')  # Permitimos division entre cero
    ndvi = np.divide((nir - red), (nir + red))
    #ndvi = (red - nir) / (red + nir)
    return ndvi

def apply_custom_colormap(image_gray, cmap = col_Bar.cmapColores_1):
    '''Apply a custom colormap to a grayscale image and output the
    result as a BGR image.

    Credit: https://stackoverflow.com/a/52498778
    '''
    assert image_gray.dtype == np.uint8, 'must be np.uint8 image'
    if image_gray.ndim == 3:
        image_gray = image_gray.squeeze(-1)#-1

    ''' inicializar el mapa de colores '''
    sm = plt.cm.ScalarMappable(cmap=cmap)

    '''   Obtenemos un rango de colores   '''
    color_range = sm.to_rgba(np.linspace(
        0, 1, 256))[:, 0:3]  # color range RGBA => RGB
    color_range = (color_range * 255.0).astype(np.uint8)  # [0,1] => [0,255]
    #color_range = np.squeeze( np.dstack([color_range[:, 2], color_range[:, 1], color_range[:, 0]]),0)  # RGB => BGR

    # Apply colormap for each channel individually
    channels = [cv2.LUT(image_gray, color_range[:, i]) for i in range(3)]
    return np.dstack(channels)

'''      FUNCIÓN QUE GUARDA IMAGEN USANDO LA HORA Y CECHA COMO NOMBRE   '''
def save_image( ndvi_colorized1 , rut):
    '''    Directorio de guardado    '''
    directorio = rut
    '''    Para quitar los valores de los ejes     '''
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.gca().axes.get_xaxis().set_visible(False)
    # plt.hist(ndvi_colorized1.ravel(),256,[0,256]);
    '''    Mostramos la imagen con la paleta de colores   '''
    plt.imshow(ndvi_colorized1, cmap = col_Bar.cmapColores_BAR)
    plt.colorbar()
    plt.title('NDVI')
    plt.clim(vmax=0 , vmin=1)
    plt.savefig(directorio,dpi=256)#2600
    plt.close(1)
    return directorio

def imageProcessing (NDVI):
    '''    Coloreamos la imagen   '''
    ndvi_colorized = apply_custom_colormap ( 255 * NDVI.astype(np.uint8), cmap = col_Bar.cmapColores_1 )
    '''    Guardamos imagen y devolvemos direccion de la imágen NDVI     '''
    guardarRutaCategoria = myPath.getPathByCategory('NDVI')
    return save_image( ndvi_colorized , guardarRutaCategoria)

        
    
    


