# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ventana_REALCEBORDES.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QPushButton
from realce_D_Bordes import realceBordes as Realce
from NDVI_files import rutas_D_Archivos as paths
from TipoIMG import configuracion as extentionImg

class Ui_VENTANArealceDeBordes(object):
    def setupUi(self, VENTANArealceDeBordes):
        VENTANArealceDeBordes.setObjectName("VENTANArealceDeBordes")
        VENTANArealceDeBordes.resize(760, 590)
        VENTANArealceDeBordes.setMinimumSize(QtCore.QSize(760, 190))
        VENTANArealceDeBordes.setMaximumSize(QtCore.QSize(760, 590))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        VENTANArealceDeBordes.setFont(font)
        VENTANArealceDeBordes.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        VENTANArealceDeBordes.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.centralwidget = QtWidgets.QWidget(VENTANArealceDeBordes)
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox_ACCIONES = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_ACCIONES.setGeometry(QtCore.QRect(16, 0, 141, 161))
        self.groupBox_ACCIONES.setObjectName("groupBox_ACCIONES")
        self.btn_ABRIRORIGINAL = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_ABRIRORIGINAL.setGeometry(QtCore.QRect(14, 20, 111, 30))
        self.btn_ABRIRORIGINAL.setObjectName("btn_ABRIRORIGINAL")
        self.btn_ABRIRFILTRADA = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_ABRIRFILTRADA.setGeometry(QtCore.QRect(14, 70, 111, 30))
        self.btn_ABRIRFILTRADA.setObjectName("btn_ABRIRFILTRADA")
        self.btn_REALCEBORDES = QtWidgets.QPushButton(self.groupBox_ACCIONES)
        self.btn_REALCEBORDES.setGeometry(QtCore.QRect(14, 120, 111, 30))
        self.btn_REALCEBORDES.setObjectName("btn_REALCEBORDES")
        self.groupBox_IMG_ORIGINAL = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_IMG_ORIGINAL.setGeometry(QtCore.QRect(186, 0, 261, 171))
        self.groupBox_IMG_ORIGINAL.setObjectName("groupBox_IMG_ORIGINAL")
        self.photo_ORIGINAL_2 = QtWidgets.QLabel(self.groupBox_IMG_ORIGINAL)
        self.photo_ORIGINAL_2.setGeometry(QtCore.QRect(10, 25, 241, 131))
        self.photo_ORIGINAL_2.setText("")
        self.photo_ORIGINAL_2.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_ORIGINAL_2.setScaledContents(True)
        self.photo_ORIGINAL_2.setObjectName("photo_ORIGINAL_2")
        self.groupBox_IMG_RESULTANTE = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_IMG_RESULTANTE.setGeometry(QtCore.QRect(16, 170, 731, 381))
        self.groupBox_IMG_RESULTANTE.setObjectName("groupBox_IMG_RESULTANTE")
        self.photo_RESULTANTE_2 = QtWidgets.QLabel(self.groupBox_IMG_RESULTANTE)
        self.photo_RESULTANTE_2.setGeometry(QtCore.QRect(10, 20, 711, 351))
        self.photo_RESULTANTE_2.setText("")
        self.photo_RESULTANTE_2.setPixmap(QtGui.QPixmap("../imgs/spiderman.jpg"))
        self.photo_RESULTANTE_2.setScaledContents(True)
        self.photo_RESULTANTE_2.setObjectName("photo_RESULTANTE_2")
        self.groupBox_HISTOGRAMA = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_HISTOGRAMA.setGeometry(QtCore.QRect(486, 0, 261, 171))
        self.groupBox_HISTOGRAMA.setObjectName("groupBox_HISTOGRAMA")
        self.photo_FILTRADA = QtWidgets.QLabel(self.groupBox_HISTOGRAMA)
        self.photo_FILTRADA.setGeometry(QtCore.QRect(10, 20, 241, 141))
        self.photo_FILTRADA.setText("")
        self.photo_FILTRADA.setPixmap(QtGui.QPixmap("../imgs/IMG_1593142839540_HISTOGRAMA.TIF"))
        self.photo_FILTRADA.setScaledContents(True)
        self.photo_FILTRADA.setObjectName("photo_FILTRADA")
        VENTANArealceDeBordes.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(VENTANArealceDeBordes)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 760, 23))
        self.menubar.setObjectName("menubar")
        VENTANArealceDeBordes.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(VENTANArealceDeBordes)
        self.statusbar.setObjectName("statusbar")
        VENTANArealceDeBordes.setStatusBar(self.statusbar)

        self.retranslateUi(VENTANArealceDeBordes)
        self.btn_ABRIRORIGINAL.clicked.connect(self.show_ORIGINAL)
        self.btn_ABRIRFILTRADA.clicked.connect(self.show_FILTRADA)
        self.btn_REALCEBORDES.clicked.connect(self.mostrarREALCEdBORDES)
        QtCore.QMetaObject.connectSlotsByName(VENTANArealceDeBordes)

    def retranslateUi(self, VENTANArealceDeBordes):
        _translate = QtCore.QCoreApplication.translate
        VENTANArealceDeBordes.setWindowTitle(_translate("VENTANArealceDeBordes", "Realce de bordes"))
        self.groupBox_ACCIONES.setTitle(_translate("VENTANArealceDeBordes", "Acciones"))
        self.btn_ABRIRORIGINAL.setText(_translate("VENTANArealceDeBordes", "Imagen Original"))
        self.btn_ABRIRFILTRADA.setText(_translate("VENTANArealceDeBordes", "Imagen filtrada"))
        self.btn_REALCEBORDES.setText(_translate("VENTANArealceDeBordes", "Realzar bordes"))
        self.groupBox_IMG_ORIGINAL.setTitle(_translate("VENTANArealceDeBordes", "Imagen original"))
        self.groupBox_IMG_RESULTANTE.setTitle(_translate("VENTANArealceDeBordes", "Imagen Procesada"))
        self.groupBox_HISTOGRAMA.setTitle(_translate("VENTANArealceDeBordes", "Imagen filtrada"))
    
    def show_ORIGINAL (self):
        global fileNameORIGINAL
        
        fileNameORIGINAL = self.abrir_imagen()
        self.photo_ORIGINAL_2.setPixmap(QtGui.QPixmap( fileNameORIGINAL ))
        
    def show_FILTRADA (self):
        global fileNameFILTRADA
        
        fileNameFILTRADA = self.abrir_imagen()
        self.photo_FILTRADA.setPixmap(QtGui.QPixmap( fileNameFILTRADA ))
        
    def mostrarREALCEdBORDES (self):
        try:
            imageREALCE = Realce.getEdgeEnhancement( fileNameORIGINAL, fileNameFILTRADA )
            self.photo_RESULTANTE_2.setPixmap(QtGui.QPixmap( imageREALCE ))
        except:
            self.clickMethod()
                
    def clickMethod(self):
        q = QMessageBox(QMessageBox.Warning, extentionImg.typeAlert, extentionImg.typeMessage)
        q.setStandardButtons(QMessageBox.Ok);
        q.exec_()
                
    def abrir_imagen(self):
        title = extentionImg.titulo
        qfd = QFileDialog()
        path = paths.PATH
        filter = extentionImg.filtroIMG
        f, _ = QFileDialog.getOpenFileName(qfd, title, path, filter)
        return str(f)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    VENTANArealceDeBordes = QtWidgets.QMainWindow()
    ui = Ui_VENTANArealceDeBordes()
    ui.setupUi(VENTANArealceDeBordes)
    VENTANArealceDeBordes.show()
    sys.exit(app.exec_())

