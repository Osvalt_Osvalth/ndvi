+CALCULO DEL NDVI
+
+Interfece.
+
+LA interface esta hecha con el uso de GUI QT Desinger en la version 5
+
+Python.
+
+Para el uso de los métodos, se ha implementado en python, separandolos de 
+la interface e importandolos dentro de alla como módulos para hacer de 
+manera más separada.

Comando para la conversión del archivo .ui a código Python mediante PyUIC, 
atraves del comando pyuic5 -x (NombreArchivo.ui)  -o (NombreArchivo.py)

##DEPENDENCIES

##PYTHON AND ANACONDA UPDATE 
python -m pip install -U pip  

##UPDATE CONDA
conda update --all

##INSTALL SPYDER
conda install spyder

##INSTALL OPENCV
pip install opencv-python